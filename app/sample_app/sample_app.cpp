#include <stdio.h>
#include <unistd.h>			/* exit() */
#include <assert.h>
#include "sgx_urts.h"
#include "shim_layer.h"

#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include<sys/types.h>
#define PORT 8080

extern sgx_enclave_id_t enclave_id;

int main(int argc, char *argv[])
{
	if(shim_init() != 0){
		printf("unable to initialize shim layer\n");
		exit(0);
	}

	printf("Welcome to sample application\n");


	int returned_value;
	sgx_status_t ecall_success;

	ecall_success = ecall_enclave_main(enclave_id, &returned_value);
	assert(ecall_success == SGX_SUCCESS);

	if(shim_terminate() != 0){
		printf("unable to terminate shim layer\n");
		exit(0);
	}
	return returned_value;
}



	/******************************************************
 	*************** OLD CODE used for testing *************
	*******************************************************/



// void *server_thread(void* a){
// 	//server
// 	int server_fd, new_socket, valread;
// 	struct sockaddr_in address;
// 	int opt = 1;
// 	int addrlen = sizeof(address);
// 	char buffer[1024] = {0};
// 	char *hello = "Hello from server";
//
// 	// Creating socket file descriptor
// 	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
// 	{
// 		perror("socket failed");
// 		exit(EXIT_FAILURE);
// 	}
//
// 	// Forcefully attaching socket to the port 8080
// 	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
// 				&opt, sizeof(opt)))
// 	{
// 		perror("setsockopt");
// 		exit(EXIT_FAILURE);
// 	}
// 	address.sin_family = AF_INET;
// 	address.sin_addr.s_addr = INADDR_ANY;
// 	address.sin_port = htons( PORT );
//
// 	// Forcefully attaching socket to the port 8080
// 	if (bind(server_fd, (struct sockaddr *)&address,
// 				sizeof(address))<0)
// 	{
// 		perrosource /opt/intel/sgxsdk/environmentr("bind failed");
// 		exit(EXIT_FAILURE);
// 	}
// 	if (listen(server_fd, 3) < 0)
// 	{
// 		perror("listen");
// 		exit(EXIT_FAILURE);
// 	}
// 	if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
// 					(socklen_t*)&addrlen))<0)
// 	{
// 		perror("accept");
// 		exit(EXIT_FAILURE);
// 	}
// 	valread = read( new_socket , buffer, 1024);
// 	write(1,buffer,20);
// 	send(new_socket , hello , strlen(hello) , 0 );
// 	write(1,"Hello message sent\n",20);
// }
//
// void *thread_fun1_app(void* a){
// 	 thread_fun1(enclave_id,6);
// }
// void *ocall_thread_app(void* a){
// 	// thread_fun2(enclave_id);
// }

// void signal_callback_handler(int signum)
// {
// 	printf("Caught signal %d\n",signum);
// 	exit(signum);
// }
//
// void signal_callback_handler2(int signum)
// {
// 	printf("2222Caught signal 22%d\n",signum);
// 	exit(signum);
// }
//
// void ocall_thread(){
//
// 	signal(SIGINT, signal_callback_handler);
// 	signal(SIGINT, signal_callback_handler2);
// 	while(1)
// 	{
// 			printf("Program processing stuff here.\n");
// 			sleep(1);
// 	}
// 	return;
//
// 	pthread_t thread_id;
// 	pthread_create(&thread_id, NULL, ocall_thread_app, NULL);
// 	// pthread_join(thread_id, NULL);
// 	return;
// }
