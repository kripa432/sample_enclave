#ifndef _SHIM_LAYER_H
#define _SHIM_LAYER_H

#include "enclave_u.h"
#include <signal.h>

extern sgx_enclave_id_t enclave_id;    /* global enclave id */

#define p(x) {printf("<%d>\n",x);}

extern void* sgx_sig_handler_ptr;
void outside_sig_handler(int signum, siginfo_t* siginfo, void *priv);

#if defined(__cplusplus)
extern "C" {
#endif

int shim_init();
int shim_terminate();

#if defined(__cplusplus)
}
#endif

extern long *arg0;
extern long *arg1;
extern long *arg2;
extern long *arg3;
extern long *arg4;
extern long *arg5;
extern long *arg6;

extern long *signo;


#endif /* !_SHIM_LAYER_H */
