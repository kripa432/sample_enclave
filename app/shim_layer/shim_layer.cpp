
/*
 * app/shim_layer/shim_layer.cpp
 *
 * Kripa Shanker <kripashanker@iisc.ac.in>
 *
 * This is user part of shim layer.
 *
 * Shim layer creates the enclave and initialize the buffers
 * which will be used used by enclave to copy arguments of system calls
 * outside the enclave.
 */


#include <stdio.h>
#include <assert.h>

#include "sgx_urts.h"					// for sgx_create_enclave()
#include "shim_layer.h"

#define BUF_SIZE 1000
sgx_enclave_id_t enclave_id = NULL;

long *arg0 = NULL;
long *arg1 = NULL;
long *arg2 = NULL;
long *arg3 = NULL;
long *arg4 = NULL;
long *arg5 = NULL;
long *arg6 = NULL;

long *signo = NULL;
void *sgx_sig_handler_ptr = NULL;

int shim_init()
{	// int x = 5 / b;
	sgx_status_t enclave_created, ecall_success;
	sgx_launch_token_t launch_token;
	int launch_token_updated;
	int ret;

	arg0 = (long*)malloc(BUF_SIZE);
	arg1 = (long*)malloc(BUF_SIZE);
	arg2 = (long*)malloc(BUF_SIZE);
	arg3 = (long*)malloc(BUF_SIZE);
	arg4 = (long*)malloc(BUF_SIZE);
	arg5 = (long*)malloc(BUF_SIZE);
	arg6 = (long*)malloc(BUF_SIZE);


	signo = (long*)malloc(sizeof(long));

	memset(&launch_token, 0, sizeof(sgx_launch_token_t));

	/* create enclave */
	enclave_created = sgx_create_enclave("enclave.signed.so", SGX_DEBUG_FLAG, &launch_token,
					&launch_token_updated, &enclave_id, NULL);
	//assert(status == SGX_SUCCESS);
	if (enclave_created != SGX_SUCCESS){
		printf("ERR: status = %x run: systemctl status aesmd.service\n",enclave_created);
		return -1;
	}

	ecall_success = ecall_init_transfer(enclave_id, &ret, arg0, arg1, arg2, arg3,
			arg4, arg5, arg6, signo);
	assert(ecall_success == SGX_SUCCESS);

	/* for sig_handler function pointer */
	struct sigaction act_old;
	sigaction(SIGFPE, NULL, &act_old);
	sgx_sig_handler_ptr =  (void*)act_old.sa_sigaction;

	return 0;
}

int shim_terminate()
{
	/* Destroy the enclave */
	sgx_status_t enclave_destroyed;
	enclave_destroyed = sgx_destroy_enclave(enclave_id);
	if(enclave_destroyed != SGX_SUCCESS){
		printf("ERR: unable to destroy enclave with enclave_id %ld, error_code: %x\n", enclave_id, enclave_destroyed);
		return -1;
	}

	return 0;
}

void outside_sig_handler(int signum, siginfo_t* siginfo, void *priv)
{
	*signo = signum;
	void (*fun_ptr)(int, siginfo_t*, void *) = (void (*) (int, siginfo_t*, void *)) sgx_sig_handler_ptr;
	(*fun_ptr)(signum, siginfo, priv);
	return;
}
    /*  OLD code   */
// _Z11sig_handleriP9siginfo_tPv
// test_a++;
// struct sigaction act_temp;
// sigaction(SIGFPE, NULL, &act_temp);
// (*sgx_sig_handler_ptr)(signum, siginfo, priv);
// exit(0);
