#include "shim_layer.h" 			/* for ecall_enclave_main()*/
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/utsname.h>
#include <sys/vfs.h>
#include <signal.h>
#include <grp.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <utime.h>
#include <sys/mount.h>
#include <sys/sysinfo.h>
#include <sys/times.h>
#include <sys/uio.h>

#include <bzlib.h>

#define PORT 8080
void fpe_handler(int){
	int b = 1;
}

int ecall_enclave_main()
{
	/* Test BZ2_bzReadOpen, BZ2_bzRead */
	int bzerror = 0;
	
	FILE *fp = NULL;
	BZFILE *bz_fp = NULL;

	char file_name[] = "sample.bz2";
	fp = fopen(file_name, "w");
	if(fp == NULL)
		printf("unable to open file for writing\n");
	
	int verbosity = 0;
	int work_factor = 0;
	int block_size_100k = 9;
	bz_fp = BZ2_bzWriteOpen(&bzerror, fp, block_size_100k, verbosity, work_factor);
	
	char buf[] = "keep calm and stay strong\n";
	int len = strlen(buf);
	BZ2_bzWrite(&bzerror, bz_fp, buf, len);
	int abandon = 0;
	unsigned int n_bytes_in;
	unsigned int n_bytes_out;
	BZ2_bzWriteClose(&bzerror, bz_fp, abandon, &n_bytes_in, &n_bytes_out);
	if (bzerror != BZ_OK)
		printf("err BZ2_bzReadclose\n");
	

	fp = fopen(file_name, "r");
	if(fp == NULL)
		printf("unable to open file for reading\n");
	
	int small = 1;
	bz_fp = BZ2_bzReadOpen(&bzerror, fp, verbosity, small, NULL, 0);
	if(bzerror != BZ_OK)
		printf("bzerror: %d\n", bzerror);

	int n_read = 0;
	n_read = BZ2_bzRead(&bzerror, bz_fp, buf, len);
	if(bzerror < 0){
		printf("bzerror: %d\n", bzerror);
		exit(EXIT_FAILURE);
	}

	printf("read: %d\n", n_read);
	printf("buf: %s\n", buf);


	BZ2_bzReadClose(&bzerror, bz_fp);
	if (bzerror != BZ_OK)
		printf("err BZ2_bzReadclose\n");

	if (fclose(fp) == -1)
		printf("err close\n");




	/* Test Read(0) & write(1) */
	/*
	char buf[10];
	read(STDIN_FILENO, buf, 10);
	write(STDIN_FILENO, buf, 10);
	*/

	/* Test open(2) and close(3) */
	/*
	int fd = open(".", O_RDONLY);
	if(fd < 0)
		printf("open: error\n");
	printf("open: fd %d\n", fd);
	int status = close(fd);
	if(status < 0)
		printf("close error\n");

	*/

	/* Test stat(4), fstat(5), lstat(6) */
	/*
	struct  stat sb;
	memset(&sb, 0, sizeof(sb));
	if(stat(".", &sb) == -1){
		printf("stat: error %d\n", errno);
	}
	printf("file inode number: %d\n", sb.st_ino);

	memset(&sb, 0, sizeof(sb));
	if(lstat("/home/kripa432/Music/Music", &sb) == -1){
		printf("stat: error %d\n", errno);
	}
	printf("file inode number: %d\n", sb.st_ino);

	int fd = open(".",O_RDONLY);
	memset(&sb, 0, sizeof(sb));
	if(fstat(fd, &sb) == -1){
		printf("stat: error %d\n", errno);
	}
	printf("file inode number: %d\n", sb.st_ino);
	close(fd);
	*/

	/* Test lseek(8) */
	/*
	int fd = open("/usr/include/string.h", O_RDONLY);
	off_t offset = 0;
	char buf[128];

	if(lseek(fd, offset, SEEK_SET) == -1){
		printf("lseek error\n");
	}
	read(fd, buf, 80);
	write(1, buf, 80);

	offset = 5;
	write(1,"\noffset: 5\n",11);
	if(lseek(fd, offset, SEEK_SET) == -1){
		printf("lseek error\n");
	}
	read(fd, buf, 80);
	write(1, buf, 80);
	close(fd);
	*/

	/* Test mmap(9), munmap(11) */
	/*
	char path_name[] = "sample.txt";
	int fd = open(path_name, O_RDWR);

	if(fd == -1){
		printf("mmap open failed\n");
		exit(0);
	}
	struct stat st;
	stat(path_name, &st);

	size_t length = st.st_size;


	void *addr = NULL; 	// hint address
	int prot = PROT_EXEC | PROT_READ | PROT_WRITE;
	int flags = MAP_SHARED;
	off_t offset = 0;

	void *file_mmap = mmap(addr, length, prot, flags,  fd, offset);
	if(file_mmap == MAP_FAILED){
		printf("mmap error\n");
		exit(0);
	}

	printf("sample.txt\n%s\n",file_mmap);
	scanf("%s", file_mmap);

	if(munmap(file_mmap, length) == -1)
		printf("munmap error\n");

	//printf("munmap sample.txt\n%s\n",file_mmap);
	*/

	/* Test mprotect(10) */
	/*
	size_t size = getpagesize();
	size_t alignment = getpagesize();
	int *addr = (int*)memalign(alignment, size);
	int i;
	for(i = 0; i < 1024; i++)
		addr[i] = i;
	for(i = 0; i < 1024; i++)
		printf("%d ", addr[i]);
	printf("\n");

	size_t len = size;
	if(mprotect(addr, len, PROT_NONE) == -1)
		printf("mprotect error\n");

	for(i = 0; i < 1024; i++)
		printf("%d ", addr[i]);
	printf("\n");
	*/

	/* TODO: Test rt_sigaction(13)  */

	/*
	sigset_t set;
	siginfo_t sig_info;
	timespec tout;
	// sigpending(&set);
	sigtimedwait(&set, &sig_info, &tout);
	// signal(SIGFPE, fpe_handler);
	//
	// int b = 0;
	// int x = 5 / b;
	//
	*/


	/*
	int signum = SIGFPE;
	struct sigaction act;
	struct sigaction act_old;

	act.sa_handler = &enclave_signal_handler;
	act.sa_sigaction = NULL;
	sigemptyset(&(act.sa_mask));
	act.sa_flags = SA_RESTART;
	act.sa_restorer = NULL;
	
	if(sigaction(signum, &act, &act_old) == -1)
		printf("sigaction error\n");

	int x = 5 / global_b;
	*/
	


	/* Test rt_sigprocmask(14) */
	/*
	sigset_t set, set_old;

	if(sigemptyset(&set) == -1)
		printf("sig empty set error\n");

	if(sigaddset(&set, SIGALRM) == -1)
		printf("sigaddset error\n");

	if(sigprocmask(SIG_BLOCK, &set, &set_old) == -1)
		printf("rt_sigprocmask error\n");

	if(sigprocmask(SIG_BLOCK, NULL, &set_old) == -1)
		printf("rt_sigprocmask error\n");


	if(sigprocmask(SIG_UNBLOCK, &set, &set_old) == -1)
		printf("rt_sigprocmask error\n");

	if(sigprocmask(SIG_UNBLOCK, NULL, &set_old) == -1)
		printf("rt_sigprocmask error\n");

	if(sigaddset(&set, SIGINT) == -1)
		printf("sigaddset error\n");

	if(sigprocmask(SIG_SETMASK, &set, &set_old) == -1)
		printf("rt_sigprocmask error\n");

	if(sigprocmask(SIG_SETMASK, NULL, &set_old) == -1)
		printf("rt_sigprocmask error\n");

	if(sigprocmask(SIG_SETMASK, NULL, NULL) == -1)
		printf("rt_sigprocmask error\n");
	*/


	/*
	 * Test pread64(17) and pwrite64(18)
	 *
	 * 1) create a file
	 * 2) write something into file
	 * 3) read back from file
	 * 4) print to stdout
	 * 5) delete the file
	 */
	/*
	char path_name[] = "sample_text.txt";
	int fd = open(path_name , O_RDWR | O_CREAT);
	if(fd < 0)
		printf("open error\n");
	int buf_size = 128;
	char buf[buf_size] = "Keep calm and stay strong\n";

	int offset = 0;
	if(pwrite(fd, buf, strlen(buf), offset) == -1)
		printf("pwrite error\n");

	memset(buf, 0, buf_size);
	offset = 5;
	if(pread(fd, buf, buf_size, offset) == -1)
		printf("pread error\n");
	puts(buf);
	close(fd);
	unlink(path_name);
	*/

	/* Test readv(19), writev(20) */
	/*
	int fd = open("sample", O_RDONLY);
	if(fd == -1){
		char str[] = "unable to open file";
		write(STDOUT_FILENO, str, strlen(str));
	}

	int len = 1024;
	char str0[len];
	char str1[len];
	char str2[len];
	struct iovec iov[3];
	ssize_t n_read = 0;
	ssize_t n_written = 0;
	int count = 3;

	iov[0].iov_base = str0;
	iov[0].iov_len = 1 + 1;
	iov[1].iov_base = str1;
	iov[1].iov_len = 2 + 1;
	iov[2].iov_base = str2;
	iov[2].iov_len = 3 + 1 + 10;

	n_read = readv(fd, iov, 3);

	n_written = writev(STDOUT_FILENO, iov, 3);
	*/

	/* Test access(21) */
  	/*
	char *path_name = "/usr/include/string.h";
	if(access(path_name, F_OK) == -1){
		printf("acess: file not exist\n");
	}

	if(access(path_name, R_OK) == -1){
		printf("acess: no read permission\n");
	}

	if(access(path_name, W_OK) == -1){
		printf("acess: no write permission\n");
	}

	if(access(path_name , X_OK) == -1){
		printf("acess: no execute permission\n");
	}
	*/

	/* Test mremap(25) */
	/*
	void *old_address;
	size_t size_old;
	size_t size_new;
	int flags = 0;
	void *new_address;
	char path_name[] = "sample.txt";

	int fd = open(path_name, O_RDONLY);
	if(fd == -1)
		printf("mremap open error\n");

	struct stat st;
	if(stat(path_name, &st) == -1)
		printf("mremap stat error\n");

	size_old = st.st_size;
	old_address = mmap(NULL, size_old, PROT_READ, MAP_SHARED, fd, 0);
	if(old_address == MAP_FAILED)
		printf("mremap mmap error\n");

	printf("%s\n %s\n", path_name, old_address);

	size_new = 4096;
	new_address = mremap(old_address, size_old, size_new, flags);
	if(new_address == MAP_FAILED)
		printf("mremap failed\n");

	char buf[4096];
	strncpy(buf, (char*)new_address, 4096);
	printf("after mremap\n %s\n", buf);
	*/

	/* Test msync(26) */
	/*
	void *addr = NULL;
	size_t length;
	int flags;
	char path_name[] = "sample_msync.txt";

	int fd = open(path_name, O_CREAT | O_RDWR);
	if(fd == -1){
		printf("msync open error\n");
		exit(EXIT_FAILURE);
	}

	length = 4096;
	if(fallocate(fd, 0, 0, length) == -1)
		printf("msync fallocate error\n");

	addr = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(addr == MAP_FAILED){
		printf("msync mmap error\n");
		exit(EXIT_FAILURE);
	}

	strcpy((char*)addr, "kripa432");

	flags = MS_SYNC;
	if(msync(addr, length, flags) == -1)
		printf("msync error\n");

	if(unlink(path_name) == -1)
		printf("msync unlink error\n");
	*/


	/* Test madvise(28) */
	/*
	char path_name[] = "sample.txt";
	int fd = open(path_name, O_CREAT | O_RDWR, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);

	char str[] = "abcdefghijklmnopqrstuvwxyz12345\n";
	// writing 10 * 4KB of data into file
	// https://github.com/linux-test-project/ltp/blob/master/testcases/kernel/syscalls/madvise/madvise01.c
	int i;
	for(i = 0; i < 1280; i++ )
		write(fd, str, strlen(str));

	struct stat st;
	if(fstat(fd, &st) == -1)
		printf("madvise fstat error\n");

	// Map file into shared memory
	void *file_mmap = mmap(NULL, st.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(file_mmap == MAP_FAILED)
		printf("madvise mmap error\n");

	void *addr = file_mmap;
	size_t length = st.st_size;;
	int advise = MADV_SEQUENTIAL;

	if(madvise(addr, length, advise) == -1)
		printf("madvise error\n");

	char buf[32];
	for(i = 0; i < 1200; i++)
		strncpy(buf, (char*)file_mmap + (i * 32), 32);
	if(close(fd) == -1)
		printf("madvise close error\n");
	if(munmap(addr, st.st_size) == -1)
		printf("madvise munmap\n");
	*/

	/* Test dup(32), dup2(33) */
	/*
	int fd = dup(1);
	if(fd == -1)
		printf("dup error\n");
	write(fd, "This is new fd\n", 15);

	fd = dup2(1, 128);
	if(fd == -1)
		printf("dup2 error\n");
	write(fd, "the is dup2 fd\n", 15);
	*/


	/* Test nanosleep(35) */
	/*
	struct timespec req = {3, 0};
	nanosleep(&req, NULL);
	*/


	/* Test alarm(37) */
	/*
	int pending_alarm = alarm(5);
	sleep(10);
	*/

	/* Network system calls (41-55) */
	// //
	// int server_fd, new_socket, valread;
	// struct sockaddr_in address;
	// int opt = 1;
	// int addrlen = sizeof(address);
	// char buffer[1024] = {0};
	// char *hello = "Hello from server";
	//
	//
	// struct iovec iov[3];
	// struct msghdr msg_hd;
	// msg_hd.msg_name = NULL;//(void*) &address;
	// msg_hd.msg_namelen = 0;//sizeof(address);
	// msg_hd.msg_iov = iov;
	// msg_hd.msg_iovlen = 3;
	// msg_hd.msg_control = NULL;
	// msg_hd.msg_controllen = 0;
	// msg_hd.msg_flags = 0;
	//
	// int a=11, b=21, c=31;
	// iov[0] .iov_base = (void*)&a;
	// iov[0] .iov_len = sizeof(int);
	// iov[1] .iov_base = (void*)&b;
	// iov[1] .iov_len = sizeof(int);
	// iov[2] .iov_base = (void*)&c;
	// iov[2] .iov_len = sizeof(int);
	//
	//
	//
	// // Creating socket file descriptor
	// if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	// {
	// 	perror("socket failed");
	// 	exit(EXIT_FAILURE);
	// }
	//
	// // Forcefully attaching socket to the port 8080
	// if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
	// 			&opt, sizeof(opt)))
	// {
	// 	perror("setsockopt");
	// 	exit(EXIT_FAILURE);
	// }
	// address.sin_family = AF_INET;
	// address.sin_addr.s_addr = INADDR_ANY;
	// address.sin_port = htons( PORT );
	//
	// // Forcefully attaching socket to the port 8080
	// if (bind(server_fd, (struct sockaddr *)&address,
	// 			sizeof(address))<0)
	// {
	// 	perror("bind failed");
	// 	exit(EXIT_FAILURE);
	// }
	// if (listen(server_fd, 3) < 0)
	// {
	// 	perror("listen");
	// 	exit(EXIT_FAILURE);
	// }
	// if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
	// 				(socklen_t*)&addrlen))<0)
	// {
	// 	perror("accept");
	// 	exit(EXIT_FAILURE);
	// }
	// recvfrom(new_socket,buffer,18,0,NULL,NULL);
	// // valread = read( new_socket , buffer, 1024);
	// printf("recieved : %s",buffer);
	// // kripa432();
	// fpe_handler(1);
	// // send(new_socket , hello , strlen(hello) , 0 );
	// recvmsg(new_socket,&msg_hd, 0);
	//
	// printf("msg recieved a = %d, b = %d, c = %d\n",a,b,c);
	// write(1,"Hello message sent\n",20);
	// return 0;

	/* Test exit(60) */
	/*
	exit(0);
	printf("This should not print\n");
	*/

	/* Test uname(63) */
	/*
	struct utsname buf;
	if(uname(&buf) == -1)
		printf("uname error\n");
	printf("sysname = %s\n", buf.sysname);
	printf("namename = %s\n", buf.nodename);
	printf("release = %s\n", buf.release);
	printf("version = %s\n", buf.version);
	printf("machine = %s\n", buf.machine);
	*/

	/* Test exit(60) */
	/*
	exit(0);
	printf("This should not be printed\n");
	*/

	/* Test kill(62) */
	/*
	pid_t pid = 0;
	int sig = SIGKILL;

	if(kill(pid, sig) == -1)
		printf("kill error\n");

	printf("process %d is send signal %d\n", pid, sig);
	*/

	/* test truncate(76), ftruncate(77) */
	/*
	char path_name[] = "sample_truncate.txt";
	char str[] = "abcdefghijklmnopqrstuvwxyz12345\n";
	off_t length = 1024;

	int fd = open(path_name, O_CREAT | O_RDWR);
	if(fd == -1){
		printf("ftruncate open error\n");
		exit(EXIT_FAILURE);
	}

	int i;
	for(i = 0; i <= 1024; i++)
		write(fd, str, strlen(str));

	if(truncate(path_name, length) == -1)
		printf("truncate error\n");

	if(ftruncate(fd, length/2) == -1)
		printf("ftruncate error\n");

	if(unlink(path_name) == -1)
		printf("truncate unlink error\n");

	*/

	/* Test fsync(74) */
	/*
	int fd;
	char path_name[] = "sample_fsync.txt";
	char str[] = "abcdefghijklmnopqrstuvwxyz12345\n";

	fd = open(path_name, O_CREAT | O_RDWR);
	write(fd, str, strlen(str));

	if(fsync(fd) == -1)
		printf("fsync error\n");
	close(fd);
	if(unlink(path_name) == -1)
		printf("fsync unlink error\n");
	*/


	/* Test getdents(78) */
	/*
	struct linux_dirent64 {
		ino64_t 	d_ino; 		// 64-bit inode number
		off64_t 	d_off; 		// 64-bit offset to next structure
		unsigned char 	d_reclen;	// size of this dirent
		unsigned char 	d_type; 	// File type
		char 		d_name[]; 	// file name (null-terminated)
	};

	#define BUF_SIZE 1024

	char buf[BUF_SIZE];
	int fd = open(".", O_RDONLY | O_DIRECTORY);

	if(fd == -1)
		printf("getdents open error\n");
	for( ; ; ) {
		int nread= syscall(SYS_getdents, fd, buf, BUF_SIZE);
		if(nread == -1)
			printf("getdents error\n");

		if(nread == 0)
			break;

		printf("---------------------------------nread = %d----------------\n", nread);
		printf("inode#\tfile type\td_reclen\d_off\td_name\n");

		int bpos;
		for(bpos = 0; bpos < nread; ){
			struct linux_dirent64 *d = (struct linux_dirent64 *)(buf + bpos);
			printf("%8ld ",d->d_ino);
			printf("%s\n",d->d_name);
			bpos += d->d_reclen;
		}
	}
	*/



	/* Test getcwd(79) */
	/*
	int buf_size = 120;
	char buf[buf_size];

	if(getcwd(buf, buf_size) == NULL){
		printf("getcwd error\n");
	}
	puts(buf);
	*/


	/* Test chdir(80) */
	/*
	int buf_size = 128;
	char buf[buf_size];

	if(getcwd(buf, buf_size) == NULL)
		printf("chdir getcwd error\n");
	printf("cwd: %s\n", buf);

	if(chdir("..") == -1)
		printf("chdir error\n");

	if(getcwd(buf, buf_size) == NULL)
		printf("chdir getcwd error\n");
	printf("cwd: %s\n", buf);
	*/

	/* Test fchdir(81) */
	/*
	int fd = open("../..", O_RDONLY);
	if(fd == -1)
		printf("fchdir open error\n");

	if(fchdir(fd) == -1)
		printf("fchdir error\n");

	int buf_size = 4096;
	char buf[buf_size];

	getcwd(&buf[0], buf_size);

	write(1, buf, strlen(buf)+1);
	close(fd);
	*/


	/* Test rename(82) */
	/*
	char path_name_old[] = "sample_old.txt";
	char path_name_new[] = "sample_new.txt";

	int fd = open(path_name_old, O_CREAT | O_RDONLY);
	if(rename(path_name_old, path_name_new) == -1)
		printf("rename error\n");

	close(fd);

	if(unlink(path_name_new) == -1)
		printf("rename unlink error\n");
	*/

	/* Test mkdir(83), rmdir(84) */
	/*
	char path_name[] = "sample_dir";
	mode_t mode = 0777;
	if(mkdir(path_name, mode) == -1)
		printf("mkdir error\n");

	if(access(path_name, R_OK) == -1)
		printf("mkdir access error\n");
	if(rmdir(path_name) == -1)
		printf("rmdir error\n");
	*/

	/* Test creat(85), link(86), unlink(87) */
	/*
	char path_name[] = "sample_text.txt";
	char buf[] = "Hello World";

	int fd = creat(path_name, O_TRUNC | O_CREAT | S_IRWXU);
	if(fd < 0)
		printf("creat error\n");
	write(fd, buf, strlen(buf));
	close(fd);

	char *path_name_old = path_name;
	char path_name_new[] = "sample_text_link.txt";
	if(link(path_name_old, path_name_new) == -1)
		printf("link error\n");

	if(unlink(path_name_old) == -1)
		printf("unlink error\n");

	if(unlink(path_name_new) == -1)
		printf("unlink error\n");
	*/


	/* Test symlink(88) */
	/*
	char path_target[] = "/tmp";
	char path_link[] = "link_to_tmp";

	if(symlink(path_target, path_link) == -1)
		printf("symlink error\n");

	if(unlink(path_link) == -1)
		printf("symlink unlink error\n");
	*/


	/* Test readlink(89) */
	/*
	size_t buf_size = 128;
	char path_name[buf_size];	// symbolic link
	char buf[buf_size];		// symbolic link content

	strcpy(path_name, "/home/kripa432/Music/Music");	// path_name is symblic link on computer

	if(readlink(path_name, buf, buf_size) == -1)
		printf("readlink error\n");
	printf("symbolic link content %s\n", buf);
	*/

	/* Test chmod(90) */
	/*
	char path_name[] = "sample_text.txt";
	int fd = open(path_name, O_CREAT, O_RDONLY);
	close(fd);

	if(chmod(path_name, S_IROTH | S_IWOTH | S_IXOTH) == -1)
		printf("chmod error\n");

	if(unlink(path_name) == -1)
		printf("chmod unlink error\n");
	*/

	/* Test fchmod(91) */
	/*
	char path_name[] = "sample_chmod.txt";
	int fd = open(path_name, O_CREAT | O_RDWR);

	if(fd == -1){
		printf("fchmod open error\n");
		exit(EXIT_SUCCESS);
	}

	mode_t mode = S_IRUSR | S_IWUSR | S_IXUSR | S_IWGRP;

	if(fchmod(fd, mode) == -1)
		printf("fchmod error\n");

	close(fd);

	if(unlink(path_name) == -1)
		printf("fchmod unlink error\n");
	*/

	/* Test chown(92), fchown(93), lchown(94) */
	/*
	uid_t owner = 0;
	gid_t group = 1001;
	char path_name[] = "a.out";

	getchar();

	if(chown(path_name, owner, group) == -1)
		printf("chown error\n");
	int fd = open(path_name, O_RDONLY);
	if(fd == -1)
		printf("fchown error\n");

	getchar();

	group = 1002;
	if(fchown(fd, owner, group) == -1)
		printf("fchown error\n");

	char path_name_link[] = "Link to a.out";
	if(symlink(path_name, path_name_link) == -1)
		printf("lchown error\n");

	getchar();

	group = 1003;
	if(lchown(path_name_link, owner, group) == -1)
		printf("lchown error\n");

	getchar();

	if(unlink(path_name_link) == -1)
		printf("lchown unlink error\n");
	*/



	/* Test umask(95) // TODO */
	/*
	mode_t mask_new = 777;
	mode_t mask_old;

	char path_name_before[] = "sample_umask_before.txt";
	int fd = open(path_name_before, O_CREAT);
	if(fd == -1)
		printf("umask open error\n");
	close(fd);

	//if(unlink(path_name_before) == -1)
	//	printf("umask unlink error");

	mask_old = umask(mask_new);


	char path_name_after[] = "sample_umask_after.txt";
	fd = open(path_name_after, O_CREAT);
	if(fd == -1)
		printf("umask open error\n");
	close(fd);

	//if(unlink(path_name_after) == -1)
	//	printf("umask unlink error");
	*/

	/* Test gettimeofday(96) TODO*/
	/*
	struct timeval tv;
	struct timezone tz;

	if(gettimeofday(&tv, &tz) == -1)
		printf("gettimeofday error\n");

	printf("seconds since epoch %lu\n", tv.tv_sec);
	*/


	/* Test getrlimit(97) */
	/*
	int resource = RLIMIT_AS;
	struct rlimit rlim;

	rlim.rlim_cur = 0;
	rlim.rlim_max = 0;

	if(getrlimit(resource, &rlim) == -1)
		printf("getrlimit error\n");

	printf("soft limit %d\n", rlim.rlim_cur);
	printf("HARD limit %d\n", rlim.rlim_max);
	*/

	/* Test getrusage(98) */
	/*
	char str[1000000];
	str[10000] = 'a';
	int who = RUSAGE_SELF;
	struct rusage usage;
	memset(&usage, 0, sizeof(usage));

	if(getrusage(who, &usage) == -1)
		printf("getrusage error\n");

	printf("stack size %ld\n", usage.ru_isrss);
	*/

	/* Test sysinfo(99) */
	/*
	struct sysinfo info;

	if(sysinfo(&info) == -1)
		printf("sysinfo error\n");

	printf("total usable ram %lu\n", info.totalram);
	printf("free ram %lu\n", info.freeram);
	*/

	/* Test times(100) */
	/*
	struct tms buf;

	long i;

	for(i = 0 ; i <= 100000000; i++);

	clock_t ticks = times(&buf);

	printf("user time %ld\n", buf.tms_utime);
	printf("system time %ld\n", buf.tms_stime);
	*/


	/* Test getuid(102), getgid(104) */
	/*
	printf("getuid() = %d\n", getuid());
	printf("getgid() = %d\n", getgid());
	*/


	/* Test setuid(105) */
	/*
	printf("old getuid() = %d\n", getuid());		// to verify: $id -u
	printf("old geteuid() = %d\n", geteuid());		// to verify: $id -u

	uid_t uid = 1001;
	if(setuid(uid) == -1)
		printf("setuid error\n");
	if(getuid() != uid)
		printf("uid not changed\n");

	printf("new getuid() = %d\n", getuid());
	printf("new geteuid() = %d\n", geteuid());
	*/


	/* Test setgid(106) */
	/*
	printf("old getgid() = %d\n", getgid());
	printf("old getegid() = %d\n", getegid());

	gid_t gid = 1001;
	if(setgid(gid) == -1)
		printf("setgid() error\n");
	if(getgid() != gid)
		printf("gid not changed\n");

	printf("new getgid() = %d\n", getgid());
	printf("new getegid() = %d\n", getegid());
	*/


	/* Test geteuid(107), getegid(108) */
	/*
	printf("geteuid() = %d\n", geteuid());
	printf("getegid() = %d\n", getegid());
	*/


	/* Test setpgid(109) */
	/*
	pid_t parent_pid = getppid();
	pid_t parent_gid = getpgid(parent_pid);

	printf("getpgid() - %d\n", getpgid(0));

	// warning signals are delivered to process groups
	if(setpgid(getpid() , parent_gid) == -1)
		printf("setpgid error\n");
	printf("getpgid() - %d\n", getpgid(0));
	getchar();
	// gdb will be stopped, maybe due to interupt signal
	*/


	/* Test getppid(110), getpgrp(111) */
	/*
	printf("getppid() = %d\n", getppid());
	printf("getpgrp() = %d\n", getpgrp());
	getchar();
	*/


	/* Test setsid(112)*/
	/*
	pid_t parent_pid = getppid();

	printf("getpgid() = %d\n", getpgid(0));
	if(setpgid(0, getpgid(parent_pid)) == -1)
		printf("setsid setgpid error\n");

	printf("getpgid() = %d\n", getpgid(0));

	pid_t session_id = setsid();
	printf("getpgid() = %d\n", getpgid(0));

	if(session_id == -1)
		printf("setsid error\n");
	printf("session_id %d\n", session_id);
	*/

	/* Test setreuid(113) */
	/*
	uid_t ruid = getuid();
	uid_t euid = geteuid();

	printf("getuid() = %d\n", getuid());
	printf("geteuid() = %d\n", geteuid());


	if( setreuid(euid, ruid) == -1)
		printf("setreuid error\n");

	printf("\n");
	printf("getuid() = %d\n", getuid());
	printf("geteuid() = %d\n", geteuid());
	*/


	/* Test setregid(114) */
	/*
	gid_t rgid = getgid();
	gid_t egid = getegid();

	printf("getgid() = %d\n", getgid());
	printf("getegid() = %d\n", getegid());

	if(setregid(egid, rgid) == -1)
		printf("setregid error\n");

	printf("\n");
	printf("getgid() = %d\n", getgid());
	printf("getegid() = %d\n", getegid());
	*/


	/* Test getgroups(115) */
	/*
	int size = getgroups(0, NULL);
	if(size == -1){
		printf("getgroups error\n");
		exit(1);
	}

	gid_t list[size];

	if(getgroups(size, list) == -1){
		printf("getgroups error\n");
		exit(1);
	}

	int i;
	for(i = 0; i < size; i++)
		printf("group %d\n", list[i]);
	*/


	/* Test setgroups(116) */
	/*
	int size = 5;
	gid_t list[size] = {1000, 1001, 1002, 0, 27};

	if(setgroups(size, list) == -1)
		printf("setgroups error\n");

	memset(list, 0, size * sizeof(gid_t));

	if(getgroups(size, list) == -1){
		printf("setgroups getgroups error\n");
		exit(0);
	}

	int i;
	for(i = 0; i < size; i++)
		printf("group %d\n", list[i]);
	*/


	/* Test setresuid(117),  getresuid(118) */
	/*
	uid_t ruid;
	uid_t euid;
	uid_t suid;

	if(getresuid(&ruid, &euid, &suid) == -1)
		printf("getresuid error\n");
	printf("ruid %d\n", ruid);
	printf("euid %d\n", euid);
	printf("suid %d\n", suid);

	if(setresuid(ruid, ruid, ruid) == -1)
		printf("setresuid error\n");
	printf("\n");

	if(getresuid(&ruid, &euid, &suid) == -1)
		printf("getresuid error\n");
	printf("ruid %d\n", ruid);
	printf("euid %d\n", euid);
	printf("suid %d\n", suid);
	*/

	/* Test setresgid(119), getresgid(120) */
	/*
	gid_t rgid;
	gid_t egid;
	gid_t sgid;

	if(getresgid(&rgid, &egid, &sgid) == -1)
		printf("getresgid error\n");
	printf("rgid %d\n", rgid);
	printf("egid %d\n", egid);
	printf("sgid %d\n", sgid);

	if(setresgid(rgid, rgid, rgid) == -1)
		printf("setresgid error\n");
	printf("\n");

	if(getresgid(&rgid, &egid, &sgid) == -1)
		printf("getresgid error\n");
	printf("rgid %d\n", rgid);
	printf("egid %d\n", egid);
	printf("sgid %d\n", sgid);
	*/


	/* Test getpgid(121) */
	/*
	pid_t pid = getppid();
	printf("getpgid() = %d\n", getpgid(pid));
	getchar();
	*/

	/* Test setfsuid(122) */

	/* Test setfsgid(123) */


	/* getsid(124) */
	/*
	pid_t pid = 28891;
	pid_t  session_id = getsid(pid);
	if(session_id == -1){
		printf("getsid error\n");
		exit(0);
	}
	printf("session id %d\n", session_id);
	*/

	/* Test utime(132), utimes(235) */
	/*
	char file_name[] = "sample.txt";
	struct utimbuf times;

	if(syscall(SYS_utime, file_name, NULL) == -1)
		printf("utime error\n");


	if(syscall(SYS_utimes, file_name, NULL) == -1)
		printf("utime error\n");
	*/


	/* Test statfs(137), fstatfs(138) */
	/*
	char path_name[] = ".";
	struct statfs buf;

	if(statfs(path_name, &buf) == -1)
		printf("statfs error");
	printf("statfs\n");
	printf("Total file nodes %lu\n", buf.f_files);		// to verify: $ df -i
	printf("free file nodes %lu\n", buf.f_ffree);

	int fd = open(path_name, O_RDONLY);

	memset(&buf, 0, sizeof(buf));
	if(fstatfs(fd, &buf) == -1)
		printf("fstat error\n");

	printf("statfs\n");
	printf("Total file nodes %lu\n", buf.f_files);		// to verify: $ df -i
	printf("free file nodes %lu\n", buf.f_ffree);
	*/

	/* Test getpriority(140), setpriority(141) */
	/*
	int which = PRIO_PROCESS;
	id_t who = 0;
	errno = 0;
	int priority = 1;

	if(setpriority(which, who, priority) == -1)
		printf("setpriority error\n");

	priority = getpriority(which, who);
	if(priority == -1 && errno != 0)
		printf("getpriority error\n");

	printf("getpriority %d\n", priority);
	*/

	/* Test chroot(161) */
	/*
	char path[] = ".";

	int buf_size = 1024;
	char buf[buf_size];
	getcwd(&buf[0], buf_size);
	printf("getcwd %s\n",buf );

	if(chroot(path) == -1)
		printf("chroot error\n");


	getcwd(&buf[0], buf_size);
	printf("getcwd %s\n",buf );
	*/

	/* Test sync(162) */
	/*
	char path_name[] = "sample_sync.txt";
	int fd = open(path_name, O_CREAT | O_RDWR);

	char *file_mmap = (char*)mmap(NULL, 1024, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	file_mmap[0] = 'k';

	char buf[1024] = "hello world";
	write(fd, buf , 3);
	printf("Hello World");

	sync();

	if(unlink(path_name) == -1)
		printf("sync unlink error\n");
	*/

	/* Test mount(165), umount2(166) */
	/*
	char source[] = "/dev/sda8";
	char target[] ="/home/kripa432/Beta";
	char file_system_type[] = "ext4";
	unsigned long mount_flags = MS_RDONLY;
	void *data = NULL;

	if(mount(source, target, file_system_type, mount_flags, data) == -1){
		printf("mount error\n");
	}

	printf("press enter to continue:\n");
	getchar();
	int flags = 0;
	if(umount2(target, flags) == -1)
		printf("umount2 error\n");
	*/


	/* Test gettid(186) */
	/*
	pid_t pid = syscall(SYS_gettid);
	printf("gettid %d\n", pid);
	*/


	/* Test clock_settime(227), clock_gettime(228), clock_getres(229), clock_nanosleep(230) */
	/*
	clockid_t clk_id = CLOCK_REALTIME;

	struct timespec tp;

	if(syscall(SYS_clock_gettime, clk_id, &tp) == -1)
		printf("clock_gettime error\n");

	printf("seconds since epoch %ld\n", tp.tv_sec);


	struct timespec res;

	res.tv_sec = 0;
	res.tv_nsec = 0;

	if(clock_getres(clk_id, &res) == -1)
		printf("clock_getres error\n");

	printf("seconds %d nanoseconds %d\n", res.tv_sec, res.tv_nsec);
	*/



	/* Test exit_group(231) */
	/*
	exit(0);
	printf("This should not be printed\n");
	*/

	/* Test openat(257) */
	/*
	int dirfd = open(".", O_RDONLY);
	if(dirfd == -1)
		printf("openat open error\n");

	int fd = openat(dirfd, "sample.txt", O_RDONLY);
	if(fd == -1)
		printf("openat error\n");

	int buf_size = 1024;
	char buf[buf_size];

	buf_size = read(fd, buf, buf_size);
	write(1, buf, buf_size);
	*/

	/* Test fchownat(260) */
	/*
	int dirfd = open(".", O_RDONLY);
	if(dirfd == -1)
		printf("fchownat open error\n");

	char path_name[] = "sample_own.txt";

	int fd = open(path_name, O_CREAT);
	close(fd);

	uid_t owner = 1001;
	gid_t group = 1002;
	int flags = 0;

	if(fchownat(dirfd, path_name, owner, group, flags) == -1)
		printf("fchownat error\n");

	printf("press enter to continue:\n");
	getchar();

	if(unlink(path_name) == -1)
		printf("fchownat unlink\n");
	*/


	/* Test futimens(261) */
	/*
	char path_name[] = "sample.txt";
	struct timespec times[2] = {{0, UTIME_NOW}, {0, UTIME_NOW}};
	int dirfd = open(".", O_RDONLY);

	if(syscall(SYS_futimesat, dirfd, path_name, NULL) == -1)
		printf("futimens error\n");
	*/


	/* Test newfstatat(262) */
	/*
	int dirfd = open(".", O_RDONLY);

	if(dirfd == -1)
		printf("newfstat dirfd error\n");

	char path_name[] = "a.out";
	struct stat st;
	int flag = 0;

	if(syscall(SYS_newfstatat, dirfd, path_name, &st, flag) == -1)
		printf("newfstat error\n");
	printf("size %d\n", st.st_size);
	*/

	/* Test unlinkat(263) */
	/*
	int dirfd = open(".", O_RDONLY);
	if(dirfd == -1)
		printf("unlinkat open error\n");

	char path_name[] = "sample_unlinkat.txt";
	int fd = open(path_name, O_CREAT);
	close(fd);

	int flags = 0;
	if(unlinkat(dirfd, path_name, flags) == -1)
		printf("unlinkat error\n");
	*/


	/* Test readlinkat(267) */
	/*
	size_t buf_size = 128;
	char path_name[buf_size] = "Music/Music"; // symbolic link on local pc
	char buf[buf_size];		// symbolic link content

	int dirfd = open("/home/kripa432", O_RDONLY);

	if(readlinkat(dirfd, path_name, buf, buf_size) == -1)
		printf("readlinkat error\n");
	printf("symbolic link content %s\n", buf);
	*/

	/* Test fchmod(268) */
	/*
	int dirfd = open(".", O_RDONLY);
	if(dirfd == -1)
		printf("fchmod open error\n");
	char path_name[] = "sample.txt";
	mode_t mode = S_IRUSR | S_IWUSR;
	int flags = 0;

	if(fchmodat(dirfd, path_name, mode, flags) == -1)
		printf("fchmodat error\n");
	*/


	/* Test faccessat(269) */
	/*
	int dirfd = open(".", O_RDONLY);
	char path_name[] = "sample.txt";

	int mode = F_OK;
	int flags = 0;

	if(faccessat(dirfd, path_name, mode, flags) == -1)
		printf("faccessat error or file not exist\n");

	mode = R_OK | W_OK | X_OK;


	if(faccessat(dirfd, path_name, mode, flags) == -1)
		printf("read or write or execute permission denied\n");
	*/



	/* Test utimensat(280) */
	/*
	char path_name[] = "sample.txt";
	int dirfd = open(".", O_RDONLY);

	struct timespec times[2] = {{0, UTIME_NOW}, {0, UTIME_NOW}};
	int flags = 0;

	if(utimensat(dirfd, path_name, NULL, flags) == -1)
		printf("utimesat error\n");

	if(utimensat(dirfd, path_name, times, flags) == -1)
		printf("utimesat error\n");

	close(dirfd);
	*/



	/* Test fallocate(285) */
	/*
	char path_name[] = "sample_fallocate.txt";
	int fd = open(path_name, O_CREAT | O_RDWR);

	if(fd == -1)
		printf("fallocate open error\n");

	mode_t mode = 0;
	off_t offset = 0;
	off_t len = 1024;
	if(fallocate(fd, mode, offset, len) == -1)
		printf("fallocate error\n");
	close(fd);
	if(unlink(path_name) == -1)
		printf("fallocate unlink error\n");
	*/


	/* Test dup3(292) TODO - fork requird */
	/*
	int fd_old = open("sample.txt", O_RDONLY);

	if(fd_old == -1)
		printf("error dup3 open\n");

	int flags = O_CLOEXEC;

	int fd_new = fd_old + 1;

	fd_new = dup3(fd_old, fd_new, flags);

	if(fd_new == -1)
		printf("dup3 error\n");

	int buf_size = 32;
	char buf[buf_size];
	read(fd_new, buf, buf_size);

	printf("%s\n", buf);
	close(fd_old);
	close(fd_new);
	*/


	/* Test prlimit64(302) */
	/*
	pid_	// int server_fd, new_socket, valread;
	// struct sockaddr_in address;
	// int opt = 1;
	// int addrlen = sizeof(address);
	// char buffer[1024] = {0};
	// char *hello = "Hello from server";
	//
	//
	// struct iovec iov[3];
	// struct msghdr msg_hd;
	// msg_hd.msg_name = NULL;//(void*) &address;
	// msg_hd.msg_namelen = 0;//sizeof(address);
	// msg_hd.msg_iov = iov;
	// msg_hd.msg_iovlen = 3;
	// msg_hd.msg_control = NULL;
	// msg_hd.msg_controllen = 0;
	// msg_hd.msg_flags = 0;
	//
	// int a=10, b=20, c=30;
	// iov[0] .iov_base = (void*)&a;
	// iov[0] .iov_len = sizeof(int);
	// iov[1] .iov_base = (void*)&b;
	// iov[1] .iov_len = sizeof(int);
	// iov[2] .iov_base = (void*)&c;
	// iov[2] .iov_len = sizeof(int);
	//
	//
	//
	// // Creating socket file descriptor
	// if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	// {
	// 	perror("socket failed");
	// 	exit(EXIT_FAILURE);
	// }
	//
	// // Forcefully attaching socket to the port 8080
	// if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
	// 			&opt, sizeof(opt)))
	// {
	// 	perror("setsockopt");
	// 	exit(EXIT_FAILURE);
	// }
	// address.sin_family = AF_INET;
	// address.sin_addr.s_addr = INADDR_ANY;
	// address.sin_port = htons( PORT );
	//
	// // Forcefully attaching socket to the port 8080
	// if (bind(server_fd, (struct sockaddr *)&address,
	// 			sizeof(address))<0)
	// {
	// 	perror("bind failed");
	// 	exit(EXIT_FAILURE);
	// }
	// if (listen(server_fd, 3) < 0)
	// {
	// 	perror("listen");
	// 	exit(EXIT_FAILURE);
	// }
	// if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
	// 				(socklen_t*)&addrlen))<0)
	// {
	// 	perror("accept");
	// 	exit(EXIT_FAILURE);
	// }
	// recvfrom(new_socket,buffer,18,0,NULL,NULL);
	// // valread = read( new_socket , buffer, 1024);
	// write(1,buffer,20);
	// kripa432();
	// // send(new_socket , hello , strlen(hello) , 0 );
	// sendmsg(new_socket,&msg_hd, 0);
	// write(1,"Hello message sent\n",20);
	// return 0t pid = getpid();
	int resource = RLIMIT_NOFILE;
	struct rlimit old_limit;

	if(prlimit(pid, resource, NULL, &old_limit) == -1)
		printf("prlimit error\n");
	printf("rlim_cur %llu\n", old_limit.rlim_cur);
	printf("rlim_max %llu\n", old_limit.rlim_max);
	*/


	/* Test syncfs(306) */
	/*
	char path_name[] = "sample_syncfs.txt";
	int fd = open(path_name, O_CREAT | O_RDWR);

	if(syncfs(fd) == -1)
		printf("syncfs failed\n");
	close(fd);

	if(unlink(path_name) == -1)
		printf("syncfs unlink error\n");
	*/


	// return 0;

	// ocall_thread();
	// thread_fun1(0);
	// sleep(1);
	// write(1,buffer[6],27);
	// write(1,buffer[1],27);
	// write(1,buffer[0],27);


	// char *a;
	// a = (char*)malloc(10);
	// uid_t uid = getuid();
	// struct stat sb;
	// struct sysinfo info;
	// sysinfo(&info);
	// FILE *fptr;
	// time_t cur_time = time(NULL);
	// fptr =
	// int fd = open("/home/panda/code/experiments/Panoply/README.md",O_RDONLY);
	//int fd = access("/home/panda/code/experiments/Panoply/README.md",R_OK);
	// int fd = creat("/home/panda/file1.f",O_CREAT);
	// fstat(fd, &sb);
	// write(fd, "aaa\n", 3);
	// read(1, name, 10);
	//write(1, "12345678\n", 10);
	//test_main();
	//puts("Hello World\nThis is kripa shanker\nHow are you\nI have waited a long time");
	//printf("This is printf %d\n",2);
	//putchar('H');
	//putchar('\n');

	//int x;

	//scanf("%d", &x);
	//char ch = getchar();
	//printf("kripa432-0");
	//printf("kripa432-1");
	//return 0;
	// sleep(2);
	// exit(0);

	// printf("hallow world\n");
	// int fd = open("/dev/cdrw", O_RDONLY | O_NONBLOCK);
	//
	// // Eject the CD-ROM drive
	// ioctl(fd, CDROMEJECT);
	//
	// close(fd);
	// sys_exit();
	// return 0;

	// server
	/*
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char buffer[1024] = {0};
	char *hello = "Hello from server";


	struct iovec iov[3];
	struct msghdr msg_hd;
	msg_hd.msg_name = NULL;//(void*) &address;
	msg_hd.msg_namelen = 0;//sizeof(address);
	msg_hd.msg_iov = iov;
	msg_hd.msg_iovlen = 3;
	msg_hd.msg_control = NULL;
	msg_hd.msg_controllen = 0;
	msg_hd.msg_flags = 0;

	int a=10, b=20, c=30;
	iov[0] .iov_base = (void*)&a;
	iov[0] .iov_len = sizeof(int);
	iov[1] .iov_base = (void*)&b;
	iov[1] .iov_len = sizeof(int);
	iov[2] .iov_base = (void*)&c;
	iov[2] .iov_len = sizeof(int);



	// Creating socket file descriptor
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	{
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	// Forcefully attaching socket to the port 8080
	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
				&opt, sizeof(opt)))
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT );

	// Forcefully attaching socket to the port 8080
	if (bind(server_fd, (struct sockaddr *)&address,
				sizeof(address))<0)
	{
		perror("bind failed");
		exit(EXIT_FAILURE);
	}
	if (listen(server_fd, 3) < 0)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}
	if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
					(socklen_t*)&addrlen))<0)
	{
		perror("accept");
		exit(EXIT_FAILURE);
	}
	recvfrom(new_socket,buffer,18,0,NULL,NULL);
	// valread = read( new_socket , buffer, 1024);
	write(1,buffer,20);
	kripa432();
	// send(new_socket , hello , strlen(hello) , 0 );
	sendmsg(new_socket,&msg_hd, 0);
	write(1,"Hello message sent\n",20);
	return 0;
	*/


	//client

	// struct sockaddr_in address;
	// int sock = 0, valread;
	// struct sockaddr_in serv_addr;
	// char *hello = "Hello from client";
	// char buffer[1024] = {0};
	// if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	// {
	// 		printf("\n Socket creation error \n");
	// 		return -1;
	// }
	//
	// memset(&serv_addr, '0', sizeof(serv_addr));
	//
	// serv_addr.sin_family = AF_INET;
	// serv_addr.sin_port = htons(PORT);
	//
	// // Convert IPv4 and IPv6 addresses from text to binary form
	// if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
	// {
	// 		printf("\nInvalid address/ Address not supported \n");
	// 		return -1;
	// }
	//
	// if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	// {
	// 		printf("\nConnection Failed \n");
	// 		return -1;
	// }
	// send(sock , hello , strlen(hello) , 0 );
	// write(1,"Hello message sent\n",20);
	// valread = read( sock , buffer, 1024);
	// write(1,buffer,20);
	// return 0;

	// #define PORT 8080

	// #define CDROMEJECT              0x5309 /* Ejects the cdrom media */

	// int kripa432(){
	// 	return 0;
	// }
	//
	// int global_b = 0;

	// void enclave_signal_handler(int sig){
	// 	printf("signal received %d\n", sig);
	// 	global_b = 1;
	// }

	// char buffer[10][100];
	// void thread_fun1(int i){
	// 	char a = 48+i;
	// 	// write(1,&a,1);
	// 	sprintf( buffer[i],"fun1_%d=%c\n",i,a);
	// 	sleep(1);
	// 	a++;
	// 	sprintf( buffer[i]+9,"fun1_%d=%c\n",i,a);
	// 	// write(1,&a,1);
	// 	sleep(1);
	// 	a++;
	// 	sprintf( buffer[i]+18,"fun1_%d=%c\n",i,a);
	// 	// write(1,&a,1);
	// 	sleep(1);
	// 	return;
	// }
	//
	// void thread_fun2(){
	// 	char a = 52;int i =1;
	// 	sprintf( buffer[1],"fun2_%d=%c\n",i,a);
	// 	sleep(1);
	// 	a++;
	// 	sprintf( buffer[1]+9,"fun2_%d=%c\n",i,a);
	// 	// write(1,&a,1);
	// 	sleep(1);
	// 	a++;
	// 	sprintf( buffer[1]+18,"fun2_%d=%c\n",i,a);
	// 	// write(1,&a,1);
	// 	sleep(1);
	// 	return;
	// }
	// void signal_callback_handler(int signum)
	// {
	// 	printf("Caught signal %d\n",signum);
	// 	exit(signum);
	// }
}
