/*
 * enclave/shim_layer/shim_layer.cpp
 *
 * Kripa Shanker <kripashanker@iisc.ac.in>
 *
 * Shim layer transition processor from enclave mode to user mode,
 * whenever there is need of system call using __syscall_wrap() function.
 *
 */

#include "shim_layer.h"
#include "sgx_trts_exception.h"
/*
 * Pointers to buffers in user memory to copy arguments of system calls from inside
 * enclave to outside and copy back results.
 */

void *arg0 = NULL;
void *arg1 = NULL;
void *arg2 = NULL;
void *arg3 = NULL;
void *arg4 = NULL;
void *arg5 = NULL;
void *arg6 = NULL;

void *signo = NULL;

sgx_thread_mutex_t syscall_mutex;
volatile uint32_t syscall_lock;


struct sigaction handler_list[65];
/*
 * inside_sig_handler()
 * signal handler inside will call the handling function
 *
 */
int inside_sig_handler(sgx_exception_info_t *info)//not being invocked
{
	int sig = *(int*)signo;
	void (*fun_ptr)(int) = handler_list[sig].sa_handler;
	if(*fun_ptr == NULL){//then the handler function is in sa_sigaction
		void (*fun_ptr2 )(int, siginfo_t*, void *) = handler_list[sig].sa_sigaction;
		(*fun_ptr2)(sig, NULL, NULL);
		return 0;
	}
	(*fun_ptr)(sig);
	return 0;
}

/*
 * initialize pointers to buffers, where buffer are in user memory
 */
int ecall_init_transfer(void *buf0, void *buf1, void *buf2, void *buf3,
		void *buf4, void *buf5, void *buf6, void *buf7)
{
	if(!(buf0 && buf1 && buf2 && buf3 && buf4 && buf5 && buf6)){
		return -1;
	}
	arg0 = buf0;
	arg1 = buf1;
	arg2 = buf2;
	arg3 = buf3;
	arg4 = buf4;
	arg5 = buf5;
	arg6 = buf6;
	signo = buf7;

	sgx_thread_mutex_init(&syscall_mutex, NULL);
	syscall_lock = 0;
	sgx_register_exception_handler(1, inside_sig_handler);
	return 0;
}
