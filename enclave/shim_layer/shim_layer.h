/************************************************
 * shim layer copy the required arguments of systemcall
 * outside the enclave and leave encalve to execute system
 * call outside enclave
 * ************************************************/

#ifndef _SHIM_LAYER_H_
#define _SHIM_LAYER_H_

#include "enclave_t.h"
#include <sgx_thread.h>
#include <signal.h>

extern sgx_thread_mutex_t syscall_mutex;
extern volatile uint32_t syscall_lock;

extern void *arg0;
extern void *arg1;
extern void *arg2;
extern void *arg3;
extern void *arg4;
extern void *arg5;
extern void *arg6;

extern void *signo;


extern struct sigaction handler_list[65];

#if defined(__cplusplus)
extern "C" {
#endif

long __syscall_wrap(long n, long a1, long a2, long a3, long a4, long a5, long a6);
int send_user(void *user_addr, void *encl_addr, int len, int prot);
int recv_user(void *user_addr, void *encl_addr, int len, int prot);

void inside_sig_handler(int sig);

#if defined(__cplusplus)
}
#endif

#endif /* !_SHIM_LAYER_H_ */
