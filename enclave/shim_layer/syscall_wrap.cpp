/*
 * enclave/shim_layer/syscall_wrap.cpp
 *
 * Kripa Shanker <kripashanker@iisc.ac.in>
 * Arun Joseph <arunj@iisc.ac.in>
 *
 * __syscall_wrap copies argument of system call from
 * enclave memory to predefined user buffer and copies back
 * result from user buffer to enclave memory.
 */

#include <stdio.h>
#include "shim_layer.h"
#include <sys/syscall.h> 		/*For __NR_*** */
#include <string.h>
#include <sys/sysinfo.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/utsname.h>
#include <sys/vfs.h>
#include <signal.h>
#include <sys/socket.h>
#include <errno.h>
#include <grp.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <utime.h>
#include <sys/sysinfo.h>
#include <sys/times.h>


long __syscall_wrap(long n, long a1, long a2, long a3, long a4, long a5, long a6)
{

	int syscall_wrap_defined = 1;

	// sgx_thread_mutex_lock (&syscall_mutex);
	while(syscall_lock == 1);
	syscall_lock = 1;
	send_user(arg0, &n, sizeof(long), 0);

	switch(n){


		case __NR_read:		// read(0)
		{
			long fd = a1;
			void *buf = (void*)a2;
			long count = a3;

			send_user(arg1, &fd, sizeof(long), 0);
			send_user(arg3, &count, sizeof(long), 0);
			ocall_syscall();

			count = *(long*)arg0;
			recv_user(arg2, buf, count, 0);
			break;
		}

		case __NR_write: 	// write(1)
		{
			long fd = a1;
			void *buf = (void*)a2;
			long count = a3;

			send_user(arg1, &fd, sizeof(long), 0);
			send_user(arg2, buf, a3, 0);
			send_user(arg3, &count, sizeof(long), 0);
			ocall_syscall();
			break;
		}

		case __NR_open:		// open(2)
		{
			char *path_name = (char*)a1;
			long flags = a2;
			long mode = a3;

			send_user(arg1, path_name, strlen(path_name)+1, 0);
			send_user(arg2, &flags, sizeof(long), 0);
			send_user(arg3, &mode, sizeof(long), 0);
			ocall_syscall();
			break;
		}

		case __NR_close:	// close(3)
		{
			long fd = a1;

			send_user(arg1, &fd, sizeof(long), 0);

			ocall_syscall();
			break;
		}

		case __NR_stat:		// stat(4)
		case __NR_lstat:	// lstat(6)
		{
			void *path_name = (void*)a1;
			struct stat *buf = (struct stat*)a2;

			send_user(arg1, path_name, strlen((const char*)path_name)+1, 0);
			memset(buf, 0, sizeof(*buf));

			ocall_syscall();

			if(*(long*)arg0 != -1){
				recv_user(arg2, buf, sizeof(*buf), 0);
			}


			break;
		}

		case __NR_fstat:	// fstat(5)
		{
			long fd = a1;

			send_user(arg1, &a1, sizeof(long), 0);
			ocall_syscall();
			recv_user(arg2, (void*)a2, sizeof(struct stat)+1, 0);
			break;
		}

		case __NR_lseek:	// lseek(8)
		{
			long fd = a1;
			long offset = a2;
			long whence = a3;

			send_user(arg1, &fd, sizeof(long), 0);
			send_user(arg2, &offset, sizeof(long), 0);
			send_user(arg3, &whence, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_mmap:		// mmap(9)
		{
			void *addr = (void*)a1;
			size_t length = (size_t)a2;
			long prot = a3;
			long flags = a4;
			long fd = a5;
			off_t offset = a6;

			send_user(arg1, &addr, sizeof(void*), 0);
			send_user(arg2, &length, sizeof(length), 0);
			send_user(arg3, &prot, sizeof(prot), 0);
			send_user(arg4, &flags, sizeof(flags), 0);
			send_user(arg5, &fd, sizeof(fd), 0);
			send_user(arg6, &offset, sizeof(offset), 0);

			ocall_syscall();

			break;
		}

		case __NR_mprotect: 	// mprotect(10)
		{
			void *addr = (void*)a1;
			size_t len = (size_t)a2;
			long prot = a3;

			send_user(arg1, &addr, sizeof(void*), 0);
			send_user(arg2, &len, sizeof(len), 0);
			send_user(arg3, &prot, sizeof(prot), 0);

			ocall_syscall();

			break;
		}

		case __NR_munmap:	// munmap(11)
		{
			void *addr = (void*)a1;
			size_t length = (size_t)a2;

			send_user(arg1, &addr, sizeof(void*), 0);
			send_user(arg2, &length, sizeof(length), 0);

			ocall_syscall();

			break;
		}

		case __NR_rt_sigaction:	// rt_sigaction(13)
		{
			long signum = a1;
			struct sigaction *act = (struct sigaction*)a2;
			struct sigaction *act_old = (struct sigaction*)a3;
			size_t sigset_size = (size_t)a4;

			send_user(arg1, &signum, sizeof(signum), 0);
			if(act == NULL){
				*(long*)arg2 = 0;
			} else {
				send_user(arg2, &arg5, sizeof(arg4), 0);
				send_user(arg5, act, sizeof(*act), 0);
				handler_list[signum].sa_sigaction = act->sa_sigaction;
				handler_list[signum].sa_handler = act->sa_handler;
			}

			if(act_old == NULL){
				*(long*)arg3 = 0;
			} else {
				send_user(arg3, &arg6, sizeof(arg5), 0);
			}
			send_user(arg4, &sigset_size, sizeof(sigset_size), 0);

			ocall_syscall();

			if(act_old != NULL)
				recv_user(arg6, act_old, 8, 0);
			break;
		}


		case __NR_rt_sigprocmask: 	// rt_sigproc_mask(14)
		{
			long how = a1;
			sigset_t *set = (sigset_t*)a2;
			sigset_t *set_old = (sigset_t*)a3;
			long sigsetsize = a4;

			send_user(arg1, &how, sizeof(long), 0);
			if(set != NULL){
				send_user(arg2, set, sizeof(*set), 0);
				*(long*)arg5 = 1;
			} else{
				*(long*)arg5 = 0;	//hack dirty code: arg2 is null
			}

			if(set_old != NULL){
				send_user(arg3, set_old, sizeof(*set_old), 0);
				*(long*)arg6 = 1;
			} else {
				*(long*)arg6 = 0;	//hack dirty code: arg3 is null
			}

			send_user(arg4, &sigsetsize, sizeof(long), 0);

			ocall_syscall();

			int error_number = errno;

			if(set_old != NULL)
				recv_user(arg3, set_old, sizeof(*set_old), 0);
			break;
		}

		case __NR_rt_sigreturn:		// rt_sigreturn(15) automatically pushed when a signal came
			break;


		case __NR_ioctl: 	// ioctl(16) 1/200
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, &a2, sizeof(long), 0);
			ocall_syscall();
			recv_user(arg3, (void*)a3, sizeof(struct winsize), 0);
			break;

		case __NR_pread64:	// pread64(17)
		case __NR_pwrite64:	// pwrite64(18)
		{
			long fd = a1;
			void *buf = (void*)a2;
			long count = a3;
			long offset = a4;

			send_user(arg1, &fd, sizeof(long), 0);
			send_user(arg3, &count, sizeof(long), 0);

			if(n == __NR_pwrite64)
				send_user(arg2, buf, count, 0);

			send_user(arg4, &offset, sizeof(long), 0);

			ocall_syscall();

			count = *(long*)arg0;
			if(n == __NR_pread64)
				recv_user(arg2, buf, count, 0);

			break;
		}

		case __NR_readv:	// readv(19)
		{
			long fd = a1;
			struct iovec *iov_ptr_in = (struct iovec*)a2;
			struct iovec *iov_ptr_out = (struct iovec*)arg2;
			long iovcnt = a3;

			long i;


			for(i = 0; i < iovcnt; i++){
				struct iovec *iov_elem_in = iov_ptr_in + i;
				struct iovec *iov_elem_out = iov_ptr_out + i;

				iov_elem_out->iov_base = NULL;
				iov_elem_out->iov_len = -1;

				iov_elem_out->iov_len = iov_elem_in->iov_len;
				ocall_malloc(iov_elem_in->iov_len, &(iov_elem_out->iov_base));
				send_user(iov_elem_out->iov_base, iov_elem_in->iov_base,iov_elem_in->iov_len,0);
			}

			send_user(arg1, &fd, sizeof(long), 0);
			// arg2 is already send above iov_ptr_out
			send_user(arg3, &iovcnt, sizeof(long), 0);

			ocall_syscall();

			for(i = 0; i < iovcnt; i++){
				struct iovec *iov_elem_in = iov_ptr_in + i;
				struct iovec *iov_elem_out = iov_ptr_out + i;

				recv_user(iov_elem_out->iov_base, iov_elem_in->iov_base,iov_elem_in->iov_len,0);
			}
			break;
		}

		case __NR_writev:	// writev(20)
		{
			long fd = a1;
			struct iovec *iov_ptr_in = (struct iovec*)a2;
			struct iovec *iov_ptr_out = (struct iovec*)arg2;
			long iovcnt = a3;

			long i;


			for(i = 0; i < iovcnt; i++){
				struct iovec *iov_elem_in = iov_ptr_in + i;
				struct iovec *iov_elem_out = iov_ptr_out + i;

				iov_elem_out->iov_base = NULL;
				iov_elem_out->iov_len = -1;

				iov_elem_out->iov_len = iov_elem_in->iov_len;
				ocall_malloc(iov_elem_in->iov_len, &(iov_elem_out->iov_base));
				send_user(iov_elem_out->iov_base, iov_elem_in->iov_base,iov_elem_in->iov_len,0);
			}

			send_user(arg1, &fd, sizeof(long), 0);
			// arg2 is already send above
			send_user(arg3, &iovcnt, sizeof(long), 0);
			ocall_syscall();
			break;
		}


		case __NR_access:	// access(21)
		{
			char *path_name = (char*)a1;
			long mode = a2;

			send_user(arg1, path_name, strlen(path_name)+1, 0);
			send_user(arg2, &mode, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_mremap:	// mremap(25)
		{
			void *old_address = (void*)a1;
			size_t old_size = (size_t)a2;
			size_t new_size = (size_t)a3;
			long flags = a4;
			void *new_address = (void*)a5;

			send_user(arg1, &old_address, sizeof(void*), 0);
			send_user(arg2, &old_size, sizeof(old_size), 0);
			send_user(arg3, &new_size, sizeof(new_size), 0);
			send_user(arg4, &flags, sizeof(flags), 0);
			send_user(arg5, &new_address, sizeof(void*), 0);

			ocall_syscall();

			break;
		}

		case __NR_msync:	// msync(26)
		{
			void *addr = (void*)a1;
			size_t length = (size_t)a2;
			long flags = a3;

			send_user(arg1, &addr, sizeof(addr), 0);
			send_user(arg2, &length, sizeof(length), 0);
			send_user(arg3, &flags, sizeof(flags), 0);

			ocall_syscall();
			break;
		}

		case __NR_madvise: 	// madvise(28)
		{
			void *addr = (void*)a1;
			size_t length = (size_t)a2;
			long advise = a3;

			send_user(arg1, &addr, sizeof(void*), 0);
			send_user(arg2, &length, sizeof(length), 0);
			send_user(arg3, &advise, sizeof(advise), 0);

			ocall_syscall();

			break;
		}

		case __NR_dup:		// dup(32)
		{
			long fd = a1;

			send_user(arg1, &fd, sizeof(fd), 0);

			ocall_syscall();

			break;
		}

		case __NR_dup2:		// dup2(33)
		{
			long fd_old = a1;
			long fd_new = a2;

			send_user(arg1, &fd_old, sizeof(long), 0);
			send_user(arg2, &fd_new, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_nanosleep:	// nanoslepp(35)
		{
			struct timespec *req = (struct timespec*)a1;
			struct timespec *rem = (struct timespec*)a2;

			if(req == NULL)
				*(long*)arg1 = 0;
			else {
				send_user(arg3, req, sizeof(*req), 0);
				send_user(arg1, &arg3, sizeof(void*), 0);
			}

			if(rem == NULL)
				*(long*)arg2 = 0;
			else{
				send_user(arg2, &arg4, sizeof(void*), 0);
			}


			ocall_syscall();


			if(rem != NULL)
				recv_user(arg4, rem, sizeof(*rem), 0);


			break;
		}

		case __NR_alarm:	// alarm(37)
		{
			long seconds = a1;

			send_user(arg1, &seconds, sizeof(seconds), 0);

			ocall_syscall();
		}

		case __NR_setitimer:	// setitimer(38)
		{
			long which = a1;
			struct itimerval *new_value = (struct itimerval*)a2;
			struct itimerval *old_value = (struct itimerval*)a3;

			send_user(arg1, &which, sizeof(which), 0);
			send_user(arg2, new_value, sizeof(*new_value), 0);
			if(old_value == NULL)
				*(long*)arg3 = 0;
			else
				send_user(arg3, &arg4, sizeof(void*), 0); 	// hack

			ocall_syscall();

			if(old_value != NULL)
				recv_user(arg4, old_value, sizeof(*old_value), 0);

			break;
		}

		case __NR_getpid:	// getpid(39)
		{
			ocall_syscall();
			break;
		}

		/*---------------------------
		*network system calls 41 -55*
		----------------------------*/
		case __NR_socket:	// socket(41)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, &a2, sizeof(long), 0);
			send_user(arg3, &a3, sizeof(long), 0);
			ocall_syscall();
			break;

		case __NR_connect:	// connect(42)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, (void*)a2, a3, 0);
			send_user(arg3, &a3, sizeof(long), 0);
			ocall_syscall();
			break;

		case __NR_accept:	// accept(43)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, (void*)a2, *(socklen_t*)a3, 0);
			send_user(arg3, (void*)a3, sizeof(socklen_t), 0);
			ocall_syscall();
			break;

		case __NR_sendto:	// sendto(44)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, (void*)a2, a3, 0);
			send_user(arg3, &a3, sizeof(long), 0);
			send_user(arg4, &a4, sizeof(long), 0);
			if(a6 != 0){
				send_user(arg5, (void*)a5, *(socklen_t*)a3, 0);
				send_user(arg6, (void*)a6, sizeof(socklen_t), 0);
			}else{
				*(long*)arg6 = 0;
			}
			ocall_syscall();
			break;

		case __NR_recvfrom:	// recvfrom(45)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg3, &a3, sizeof(long), 0);
			send_user(arg4, &a4, sizeof(long), 0);
			if(a6 != 0){
				send_user(arg5, (void*)a5, *(socklen_t*)a3, 0);
				send_user(arg6, (void*)a6, sizeof(socklen_t), 0);
			}else{
				*(long*)arg6 = 0;
			}
			ocall_syscall();
			recv_user(arg2, (void*)a2, a3, 0);
			break;

		case __NR_recvmsg:	// recvmsg(47)
		case __NR_sendmsg:	// sendmsg(46)
		{
			long sockfd = a1;
			struct msghdr *msg_ptr_in = (struct msghdr*)a2;
			struct msghdr *msg_ptr_out = (struct msghdr*)arg2;
			long flags = a3;

			memset(msg_ptr_out, 0, sizeof(struct msghdr));

			send_user(arg1, &sockfd, sizeof(long), 0);

			//sending msg in arg2
			msg_ptr_out->msg_namelen = msg_ptr_in->msg_namelen;
			if(msg_ptr_in->msg_namelen == 0)
			{
				msg_ptr_out->msg_name = 0;
			}
			else
			{
				ocall_malloc(msg_ptr_in->msg_namelen, &(msg_ptr_out->msg_name));
				if(n == __NR_sendmsg)
					send_user(msg_ptr_out->msg_name, msg_ptr_in->msg_name, msg_ptr_in->msg_namelen, 0);
			}

			msg_ptr_out->msg_iovlen = msg_ptr_in->msg_iovlen;
			if(msg_ptr_in->msg_iovlen == 0)
			{
				msg_ptr_out->msg_iov = 0;
			}
			else
			{
				ocall_malloc( sizeof(struct iovec) * (msg_ptr_in->msg_iovlen), (void**)&(msg_ptr_out->msg_iov));

				for(long i = 0; i < msg_ptr_in->msg_iovlen; i++)
				{
					struct iovec *iov_elem_in = msg_ptr_in->msg_iov+i;
					struct iovec *iov_elem_out = msg_ptr_out->msg_iov+i;

					iov_elem_out->iov_base = NULL;
					iov_elem_out->iov_len = -1;

					iov_elem_out->iov_len = iov_elem_in->iov_len;
					ocall_malloc(iov_elem_in->iov_len, &(iov_elem_out->iov_base));
					if(n == __NR_sendmsg)
						send_user(iov_elem_out->iov_base, iov_elem_in->iov_base, iov_elem_in->iov_len, 0);
				}
			}

			msg_ptr_out->msg_controllen = msg_ptr_in->msg_controllen;
			if(msg_ptr_in->msg_controllen == 0)
			{
				msg_ptr_out->msg_control = 0;
			}
			else
			{
				ocall_malloc(msg_ptr_in->msg_controllen, &(msg_ptr_out->msg_control));
				if(n == __NR_sendmsg)
					send_user(msg_ptr_out->msg_control, msg_ptr_in->msg_control, msg_ptr_in->msg_controllen, 0);
			}
			msg_ptr_out->msg_flags = msg_ptr_in->msg_flags;

			send_user(arg3, &flags, sizeof(long), 0);

			ocall_syscall();

			if(n == __NR_recvmsg)
			{
				if(msg_ptr_out->msg_namelen == 0);
				else
				{
					recv_user(msg_ptr_out->msg_name, msg_ptr_in->msg_name, msg_ptr_in->msg_namelen, 0);
				}
				if(msg_ptr_out->msg_iovlen == 0);
				else
				{
					for(long i = 0; i < msg_ptr_in->msg_iovlen; i++)
					{
						struct iovec *iov_elem_in = msg_ptr_in->msg_iov+i;
						struct iovec *iov_elem_out = msg_ptr_out->msg_iov+i;
						recv_user(iov_elem_out->iov_base, iov_elem_in->iov_base, iov_elem_in->iov_len, 0);
					}
				}
				if(msg_ptr_out->msg_controllen == 0);
				else
				{
					recv_user(msg_ptr_out->msg_control, msg_ptr_in->msg_control, msg_ptr_in->msg_controllen, 0);
				}
				 msg_ptr_in->msg_flags = msg_ptr_out->msg_flags;
			}
			break;
		}

		case __NR_shutdown:	// shutdown(48)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, &a2, sizeof(long), 0);
			ocall_syscall();
			break;

		case __NR_bind:		// bind(49)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, (void*)a2, a3, 0);
			send_user(arg3, &a3, sizeof(long), 0);
			ocall_syscall();
			break;

		case __NR_listen:	// listen(50)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, &a2, sizeof(long), 0);
			ocall_syscall();
			break;

		case __NR_getsockname:	// getsockname(51) not tested
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, (void*)a2, *(socklen_t*)a3, 0);
			send_user(arg3, (void*)a3, sizeof(socklen_t), 0);
			ocall_syscall();
			break;

		case __NR_getpeername:	// getpeername(52) not tested
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, (void*)a2, *(socklen_t*)a3, 0);
			send_user(arg3, (void*)a3, sizeof(socklen_t), 0);
			ocall_syscall();
			break;

		case __NR_socketpair:	// socketpair(53) not tested
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, &a2, sizeof(long), 0);
			send_user(arg3, &a3, sizeof(long), 0);
			ocall_syscall();
			recv_user(arg4, (void*)a4, 2*sizeof(int), 0);
			break;

		case __NR_setsockopt:	// setsockopt(54)
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, &a2, sizeof(long), 0);
			send_user(arg3, &a3, sizeof(long), 0);
			send_user(arg4, (void*)a4, a5, 0);
			send_user(arg5, &a5, sizeof(socklen_t), 0);
			ocall_syscall();
			break;

		case __NR_getsockopt:	// getsockopt(55) not tested
			send_user(arg1, &a1, sizeof(long), 0);
			send_user(arg2, &a2, sizeof(long), 0);
			send_user(arg3, &a3, sizeof(long), 0);
			send_user(arg4, (void*)a4, *(socklen_t*)a5, 0);
			send_user(arg5, (void*)a5, sizeof(socklen_t), 0);
			ocall_syscall();
			break;
		/*-------------------------
		*network system calls ENDS*
		--------------------------*/
		case __NR_exit:		// exit(60)
		{
			long status = a1;
			send_user(arg1, &status, sizeof(long), 0);
			ocall_syscall();
			break;
		}

		case __NR_kill:		// kill(62)
		{
			pid_t pid = (pid_t)a1;
			long sig = a2;

			send_user(arg1, &pid, sizeof(pid), 0);
			send_user(arg2, &sig, sizeof(sig), 0);

			ocall_syscall();

			break;
		}

		case __NR_uname: 	// uname(63)
		{
			struct utsname *buf = (struct utsname*)a1;

			ocall_syscall();

			recv_user(arg1, buf, sizeof(*buf), 0);

			break;
		}

		case __NR_fsync:	// fsync(74)
		{
			long fd = a1;

			send_user(arg1, &fd, sizeof(fd), 0);

			ocall_syscall();
			break;
		}

		case __NR_truncate: 	// truncate(76)
		{
			char *path_name = (char*)a1;
			mode_t mode = (mode_t)a2;

			send_user(arg1, path_name, sizeof(path_name)+1, 0);
			send_user(arg2, &mode, sizeof(mode), 0);

			ocall_syscall();

			break;
		}

		case __NR_ftruncate:	// ftruncate(77)
		{
			long fd = a1;
			mode_t mode = (mode_t)a2;

			send_user(arg1, &fd, sizeof(fd), 0);
			send_user(arg2, &mode, sizeof(mode), 0);

			ocall_syscall();

			break;
		}

		case __NR_getdents:	// getdents(78)
		{
			long fd = a1;
			void *dirp = (void*)a2;
			long count = a3;

			send_user(arg1, &fd, sizeof(fd), 0);
			send_user(arg3, &count, sizeof(count), 0);

			ocall_syscall();

			recv_user(arg2, dirp, count, 0);

			break;
		}


		case __NR_getcwd:	// getcwd(79)
		{
			char *buf = (char*)a1;
			long size = a2;

			send_user(arg2, &size, sizeof(long), 0);

			ocall_syscall();

			if(*(long*)arg0 != 0){
				recv_user(arg1, buf, size, 0);
			}
			break;
		}

		case __NR_chdir: 	// chdir(80)
		{
			char *path_name = (char*)a1;

			send_user(arg1, path_name, strlen(path_name)+1, 0);

			ocall_syscall();

			break;
		}


		case __NR_fchdir:	// fchdir(81)
		{
			long fd = a1;

			send_user(arg1, &fd, sizeof(fd), 0);

			ocall_syscall();
			break;
		}

		case __NR_rename:	// rename(82)
		{
			char *path_name_old = (char*)a1;
			char *path_name_new = (char*)a2;

			send_user(arg1, path_name_old, strlen(path_name_old)+1, 0);
			send_user(arg2, path_name_new, strlen(path_name_new)+1, 0);

			ocall_syscall();
			break;
		}

		case __NR_mkdir:	// mkdir(83)
		{
			char *path_name = (char*)a1;
			mode_t mode = a2;

			send_user(arg1, path_name, strlen(path_name)+1, 0);
			send_user(arg2, &mode, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_rmdir:	// rmdir(84)
		{
			char *path_name = (char*)a1;

			send_user(arg1, path_name, strlen(path_name)+1, 0);

			ocall_syscall();

			break;
		}

		case __NR_creat:	// creat(85)
		{
			char *path_name = (char*)a1;
			long mode = a2;

			send_user(arg1, path_name, strlen(path_name)+1, 0);
			send_user(arg2, &mode, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_link:		// link(86)
		{
			char *path_name_old = (char*)a1;
			char *path_name_new = (char*)a2;

			send_user(arg1, path_name_old, strlen(path_name_old)+1, 0);
			send_user(arg2, path_name_new, strlen(path_name_new)+1, 0);

			ocall_syscall();

			break;
		}

		case __NR_unlink:	// unlink(87)
		{
			char *path_name = (char*)a1;
			send_user(arg1, path_name, strlen(path_name)+1, 0);

			ocall_syscall();

			break;
		}

		case __NR_symlink: 	// symlink(88)
		{
			char *path_target = (char*)a1;
			char *path_link = (char*)a2;

			send_user(arg1, path_target, strlen(path_target)+1, 0);
			send_user(arg2, path_link, strlen(path_link)+1, 0);

			ocall_syscall();
			break;
		}

		case __NR_readlink:	// readlink(89)
		{
			char *path_name = (char*)a1;
			char *buf = (char*)a2;
			long buf_size = a3;

			send_user(arg1, path_name, strlen(path_name)+1, 0);
			send_user(arg3, &buf_size, sizeof(long), 0);

			ocall_syscall();

			buf_size = *(long*)arg0;

			recv_user(arg2, buf, buf_size, 0);
			break;
		}

		case __NR_chmod: 	// chmod(90)
		{
			char *path_name = (char*)a1;
			mode_t mode = (mode_t)a2;

			send_user(arg1, path_name, strlen(path_name)+1, 0);
			send_user(arg2, &mode, sizeof(mode_t), 0);

			ocall_syscall();

			break;
		}

		case __NR_fchmod:	// fchmod(91)
		{
			long fd = a1;
			mode_t mode = (mode_t)a2;

			send_user(arg1, &fd, sizeof(fd), 0);
			send_user(arg2, &mode, sizeof(mode), 0);

			ocall_syscall();

			break;
		}


		case __NR_chown:	// chown(92)
		case __NR_lchown:	// lchown(94)
		{
			char *path_name = (char*)a1;
			uid_t owner = (uid_t)a2;
			gid_t group = (gid_t)a3;

			send_user(arg1, path_name, strlen(path_name)+1, 0);
			send_user(arg2, &owner, sizeof(owner), 0);
			send_user(arg3, &group, sizeof(group), 0);

			ocall_syscall();

			break;
		}

		case __NR_fchown:	// fchown(93)
		{
			long fd = a1;
			uid_t owner = (uid_t)a2;
			gid_t group = (gid_t)a3;

			send_user(arg1, &fd, sizeof(fd), 0);
			send_user(arg2, &owner, sizeof(owner), 0);
			send_user(arg3, &group, sizeof(group), 0);

			ocall_syscall();

			break;
		}



		case __NR_umask:	// umask(95)
		{
			mode_t mask = (mode_t)a1;

			send_user(arg1, &mask, sizeof(mask), 0);

			ocall_syscall();

			break;
		}

		case __NR_gettimeofday:	// gettimeofday(96) not tested
			send_user(arg1, (void*)a1, sizeof(struct timeval), 0);
			send_user(arg2, (void*)a2, sizeof(struct timezone), 0);
			ocall_syscall();
			break;

		case __NR_getrlimit: 	// getrlimit(97)
		{
			long resource = a1;
			struct rlimit *rlim = (struct rlimit*)a2;

			send_user(arg1, &resource, sizeof(resource), 0);

			ocall_syscall();

			recv_user(arg2, rlim, sizeof(*rlim), 0);

			break;
		}


		case __NR_getrusage:	// getrusage(98)
		{
			long who = a1;
			struct rusage *usage = (struct rusage*)a2;

			send_user(arg1, &who, sizeof(who), 0);

			ocall_syscall();

			recv_user(arg2, usage, sizeof(*usage), 0);

			break;
		}

		case __NR_sysinfo:	// sysinfo(99)
		{
			struct sysinfo *info = (struct sysinfo*)a1;

			ocall_syscall();

			recv_user(arg1, info, sizeof(*info), 0);

			break;
		}

		case __NR_times:	// times(100)
		{
			struct tms *buf = (struct tms*)a1;

			ocall_syscall();

			recv_user(arg1, buf, sizeof(*buf), 0);

			break;
		}

		case __NR_getuid:	// getuid(102)
		{
			ocall_syscall();
			break;
		}

		case __NR_getgid:	// getgid(104)
		{
			ocall_syscall();
			break;
		}

		case __NR_setuid:	// setuid(105)
		{
			uid_t uid = (uid_t)a1;

			send_user(arg1, &uid, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_setgid:	// setgid(106) not tested
		{
			gid_t gid = (gid_t)a1;

			send_user(arg1, &gid, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_geteuid:	// geteuid(107)
		{
			ocall_syscall();
			break;
		}

		case __NR_getegid:	// getegid(108)
		{
			ocall_syscall();
			break;
		}

		case __NR_setpgid:	// setpgid(109)
		{
			pid_t pid = (pid_t)a1;
			pid_t pgid = (pid_t)a2;

			send_user(arg1, &pid, sizeof(pid), 0);
			send_user(arg2, &pgid, sizeof(pgid), 0);

			ocall_syscall();

			break;
		}

		case __NR_getppid:	// getppid(110)
		{
			ocall_syscall();

			break;
		}

		case __NR_getpgrp:	// getpgrp(111)
		case __NR_getpgid:	// getpgid(121)
		{
			pid_t pid = (pid_t)a1;

			send_user(arg1, &pid, sizeof(pid_t), 0);

			ocall_syscall();
			break;
		}

		case __NR_setsid:	// setsid(112)
		{
			ocall_syscall();
			break;
		}

		case __NR_setreuid:	// setreuid(113)
		{
			uid_t ruid = (uid_t)a1;
			uid_t euid = (uid_t)a2;

			send_user(arg1, &ruid, sizeof(ruid), 0);
			send_user(arg2, &euid, sizeof(euid), 0);

			ocall_syscall();
			break;
		}

		case __NR_setregid:	// setregid(114)
		{
			gid_t rgid = (gid_t)a1;
			gid_t egid = (gid_t)a2;

			send_user(arg1, &rgid, sizeof(rgid), 0);
			send_user(arg2, &egid, sizeof(egid), 0);

			ocall_syscall();

			break;
		}

		case __NR_getgroups: 	// getgroups(115)
		{
			long size = a1;
			gid_t *list = (gid_t*)a2;

			send_user(arg1, &size, sizeof(size), 0);

			ocall_syscall();

			if(list != NULL)
				recv_user(arg2, list, size * sizeof(gid_t), 0);
			break;
		}

		case __NR_setgroups:	// setgroups(116)
		{
			long size = a1;
			gid_t *list = (gid_t*)a2;

			send_user(arg1, &size, sizeof(size), 0);
			if(list != NULL)
				send_user(arg2, list, size * sizeof(gid_t), 0);

			ocall_syscall();

			break;
		}

		case __NR_setresuid: 	// setresuid(117)
		{
			uid_t ruid = (uid_t)a1;
			uid_t euid = (uid_t)a2;
			uid_t suid = (uid_t)a3;

			send_user(arg1, &ruid, sizeof(uid_t), 0);
			send_user(arg2, &euid, sizeof(uid_t), 0);
			send_user(arg3, &suid, sizeof(uid_t), 0);

			ocall_syscall();

			break;
		}


		case __NR_getresuid: 	// getresuid(118)
		{
			uid_t *ruid = (uid_t*)a1;
			uid_t *euid = (uid_t*)a2;
			uid_t *suid = (uid_t*)a3;

			ocall_syscall();

			recv_user(arg1, ruid, sizeof(uid_t), 0);
			recv_user(arg2, euid, sizeof(uid_t), 0);
			recv_user(arg3, suid, sizeof(uid_t), 0);

			break;
		}


		case __NR_setresgid: 	// setresgid(119)
		{
			gid_t rgid = (gid_t)a1;
			gid_t egid = (gid_t)a2;
			gid_t sgid = (gid_t)a3;

			send_user(arg1, &rgid, sizeof(gid_t), 0);
			send_user(arg2, &egid, sizeof(gid_t), 0);
			send_user(arg3, &sgid, sizeof(gid_t), 0);

			ocall_syscall();

			break;
		}


		case __NR_getresgid: 	// getresgid(120)
		{
			gid_t *rgid = (gid_t*)a1;
			gid_t *egid = (gid_t*)a2;
			gid_t *sgid = (gid_t*)a3;

			ocall_syscall();

			recv_user(arg1, rgid, sizeof(gid_t), 0);
			recv_user(arg2, egid, sizeof(gid_t), 0);
			recv_user(arg3, sgid, sizeof(gid_t), 0);

			break;
		}


		case __NR_getsid:	// getsid(124)
		{
			pid_t pid = (pid_t)a1;

			send_user(arg1, &pid, sizeof(pid), 0);

			ocall_syscall();

			break;
		}

		case __NR_rt_sigsuspend:  // rt_sigsuspend(130)
		case __NR_rt_sigpending:  // rt_sigpending(127)
		{
			sigset_t *set = (sigset_t *)a1;
			size_t sigsetsize = a2;
			send_user(arg1, set, sizeof(sigset_t), 0);
			send_user(arg2, &sigsetsize, sizeof(size_t), 0);
			ocall_syscall();
			break;
		}

		case __NR_rt_sigtimedwait:  // rt_sigpending(128) don't know how to test
		{
			sigset_t *set = (sigset_t *)a1;
			siginfo_t *info = (siginfo_t *)a2;
			timespec *timeout = (timespec *)a3;
			size_t sigsetsize = a4;

			send_user(arg1, set, sizeof(sigset_t), 0);
			send_user(arg2, info, sizeof(siginfo_t), 0);
			send_user(arg3, timeout, sizeof(timespec), 0);
			send_user(arg4, &sigsetsize, sizeof(size_t), 0);

			ocall_syscall();
			break;
		}

		case __NR_rt_sigqueueinfo:  // rt_sigqueueinfo (129) not tested
		{
			pid_t tpid = a1;
			int sig = a2;
			siginfo_t *info = (siginfo_t *)a3;

			send_user(arg1, &tpid, sizeof(pid_t), 0);
			send_user(arg2, &sig, sizeof(int), 0);
			send_user(arg3, info, sizeof(siginfo_t), 0);
			ocall_syscall();
			break;
		}

		case __NR_sigaltstack: // sigaltstack(131) not completed
		{
			stack_t *signal_stack = (stack_t *)a1;
			stack_t *old_signal_stack = (stack_t *)a2;

			// TODO malloc aacordingly, if needed as of now not implemented
			// typedef struct {
			// 	void  *ss_sp;     /* Base address of stack */
			// 	int    ss_flags;  /* Flags */
			// 	size_t ss_size;   /* Number of bytes in stack */
			// } stack_t;

			if(signal_stack != 0){
				send_user(arg1, signal_stack, sizeof(stack_t), 0);
			}
			ocall_syscall();
			if(old_signal_stack != 0){
				recv_user(arg2, old_signal_stack, sizeof(stack_t), 0);
			}
			break;
		}

		
		case __NR_utime:	// utime(132)
		{
			char *file_name = (char*)a1;
			struct utimbuf *times = (struct utimbuf*)a2;

			send_user(arg1, file_name, strlen(file_name)+1, 0);
			if(times!= NULL){
				send_user(arg2, &arg3, sizeof(void*), 0);
				send_user(arg3, times, sizeof(*times), 0);
			} else {
				*(long*)arg2 = 0;
			}

			ocall_syscall();
			break;
		}



		case __NR_personality:	// personality(135) not tested
			send_user(arg1, &a1, sizeof(long), 0);
			ocall_syscall();
			break;

		case __NR_statfs: 	// statfs(137)
		{
			char *path_name = (char*)a1;
			struct statfs *buf = (struct statfs*)a2;

			send_user(arg1, path_name, strlen(path_name)+1, 0);

			ocall_syscall();

			recv_user(arg2, buf, sizeof(*buf), 0);
			break;
		}

		case __NR_fstatfs:	// fstatfs(138)
		{
			long fd = a1;
			struct statfs *buf = (struct statfs*)a2;

			send_user(arg1, &fd, sizeof(fd), 0);

			ocall_syscall();

			recv_user(arg2, buf, sizeof(*buf), 0);

			break;
		}

		case __NR_getpriority:	// getpriority(140)
		{
			long which = a1;
			id_t who = (id_t)a2;

			send_user(arg1, &which, sizeof(which), 0);
			send_user(arg2, &who, sizeof(who), 0);

			ocall_syscall();

			break;
		}

		case __NR_setpriority:	// setpriority(141)
		{
			long which = a1;
			id_t id = (id_t)a2;
			long priority = a3;

			send_user(arg1, &which, sizeof(which), 0);
			send_user(arg2, &id, sizeof(id), 0);
			send_user(arg3, &priority, sizeof(priority), 0);

			ocall_syscall();

			break;
		}

		case __NR_chroot: 	// chroot(161)
		{
			char *path = (char*)a1;

			send_user(arg1, path, strlen(path)+1, 0);

			ocall_syscall();

			break;
		}

		case __NR_sync:		// sync(162)
		{
			ocall_syscall();

			break;
		}

		case __NR_mount:	// mount(165)
		{
			char *source = (char*)a1;
			char *target = (char*)a2;
			char *file_system_type = (char*)a3;
			unsigned long mount_flags = (unsigned long)a4;
			void *data = (void*)a5;

			send_user(arg1, source, strlen(source) + 1, 0);
			send_user(arg2, target, strlen(target) + 1, 0);
			send_user(arg3, file_system_type, strlen(file_system_type) + 1, 0);
			send_user(arg4, &mount_flags, sizeof(mount_flags), 0);
			if(data == NULL){
				*(long*)arg5 = 0;
			} else {
				send_user(arg5, &arg6, sizeof(arg6), 0);
				send_user(arg6, (char*)data, strlen((char*)data) + 1, 0);
			}

			ocall_syscall();

			break;


		}

		case __NR_umount2:	// umount2(166)
		{
			char *target = (char*)a1;
			long flags = a2;

			send_user(arg1, target, strlen(target) + 1, 0);
			send_user(arg2, &flags, sizeof(flags), 0);

			ocall_syscall();

			break;
		}


		case __NR_gettid:	// getttid(186)
		{
			ocall_syscall();

			break;
		}

		case __NR_clock_gettime:	// clock_gettime(228)
		{
			clockid_t clk_id = (clockid_t)a1;
			struct timespec *tp = (struct timespec*)a2;

			send_user(arg1, &clk_id, sizeof(clk_id), 0);

			ocall_syscall();

			recv_user(arg2, tp, sizeof(*tp), 0);
			break;

		}

		case __NR_clock_getres:	// clock_getres(229)
		{
			clockid_t clk_id = (clockid_t)a1;
			struct timespec *res = (struct timespec*)a2;

			send_user(arg1, &clk_id, sizeof(clk_id), 0);

			ocall_syscall();

			if(res != NULL)
				recv_user(arg2, res, sizeof(*res), 0);
			break;
		}


		case __NR_exit_group:	// exit_group(231)
		{
			long status = a1;
			send_user(arg1, &status, sizeof(long), 0);

			ocall_syscall();

			break;
		}

		case __NR_utimes:	// utimes(235)
		{
			char *file_name = (char*)a1;
			struct timeval *times = (struct timeval*)a2;

			send_user(arg1, file_name, strlen(file_name)+1, 0);
			if(times!= NULL){
				send_user(arg2, &arg3, sizeof(void*), 0);
				send_user(arg3, times, 2 * sizeof(*times), 0);
			} else {
				*(long*)arg2 = 0;
			}

			ocall_syscall();
			break;
		}

		case __NR_openat:	// openat(257)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			long flags = a3;
			mode_t mode = (mode_t)a3;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			send_user(arg2, path_name, strlen(path_name)+1, 0);
			send_user(arg3, &flags, sizeof(flags), 0);
			send_user(arg4, &mode, sizeof(mode), 0);

			ocall_syscall();

			break;
		}

		case __NR_fchownat:	// fchownat(260)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			uid_t owner = (uid_t)a3;
			gid_t group = (gid_t)a4;
			long flags = a5;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			send_user(arg2, path_name, strlen(path_name) + 1, 0);
			send_user(arg3, &owner, sizeof(owner), 0);
			send_user(arg4, &group, sizeof(group), 0);
			send_user(arg5, &flags, sizeof(flags), 0);

			ocall_syscall();

			break;
		}

		case __NR_futimesat:	// futimensat(261)
		{
			long fd = a1;
			char *path_name = (char*)a2;
			struct timespec *times = (struct timespec*)a3;

			send_user(arg1, &fd, sizeof(fd), 0);
			send_user(arg2, path_name, strlen(path_name)+1, 0);
			if(times == NULL)
				*(long*)arg3 = 0;
			else{
				send_user(arg3, &arg4, sizeof(arg3), 0);
				send_user(arg4, times, 2 * sizeof(*times), 0);
			}

			ocall_syscall();

			break;
		}

		case __NR_newfstatat:	// newfstatat(262)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			struct stat *st = (struct stat*)a3;
			long flags = a4;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			send_user(arg2, path_name, strlen(path_name)+1, 0);
			send_user(arg4, &flags, sizeof(flags), 0);

			ocall_syscall();

			recv_user(arg3, st, sizeof(*st), 0);
			break;
		}

		case __NR_unlinkat:	// unlinkat(263)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			long flags = a3;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			send_user(arg2, path_name, strlen(path_name) + 1, 0);
			send_user(arg3, &flags, sizeof(flags), 0);

			ocall_syscall();
			break;
		}


		case __NR_readlinkat:	// readlinkat(267)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			char *buf = (char*)a3;
			size_t buf_size = (size_t)a4;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			send_user(arg2, path_name, strlen(path_name) + 1, 0);
			send_user(arg4, &buf_size, sizeof(buf_size), 0);

			ocall_syscall();

			buf_size = *(long*)arg0;
			if(buf_size != -1)
				recv_user(arg3, buf, buf_size, 0);
			break;
		}

		case __NR_fchmodat:	// fchmodat(268)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			mode_t mode = (mode_t)a3;
			long flags = a4;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			send_user(arg2, path_name, strlen(path_name) + 1, 0);
			send_user(arg3, &mode, sizeof(mode), 0);
			send_user(arg4, &flags, sizeof(flags), 0);

			ocall_syscall();

			break;
		}

		case __NR_faccessat:	// faccessat(269)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			long mode = a3;
			long flags = a4;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			send_user(arg2, path_name, strlen(path_name) + 1, 0);
			send_user(arg3, &mode, sizeof(mode), 0);
			send_user(arg4, &flags, sizeof(flags), 0);

			ocall_syscall();

			break;
		}

		case __NR_utimensat:	// utimensat(280)
		{
			long dirfd = a1;
			char *path_name = (char*)a2;
			struct timespec *times = (struct timespec*)a3;
			long flags = a4;

			send_user(arg1, &dirfd, sizeof(dirfd), 0);
			if(path_name == NULL)
				*(long*)arg2 = 0;
			else{
				send_user(arg2, &arg5, sizeof(arg5), 0);
				send_user(arg5, path_name, strlen(path_name)+1, 0);
			}

			if(times == NULL)
				*(long*)arg3 = 0;
			else{
				send_user(arg3, &arg6, sizeof(arg5), 0);
				send_user(arg6, times, 2 * sizeof(*times), 0);
			}
			send_user(arg4, &flags, sizeof(flags), 0);

			ocall_syscall();

			break;

		}

		case __NR_fallocate:	// fallocate(285)
		{
			long fd = a1;
			long mode = a2;
			off_t offset = (off_t)a3;
			off_t len = (off_t)a4;

			send_user(arg1, &fd, sizeof(fd), 0);
			send_user(arg2, &mode, sizeof(mode), 0);
			send_user(arg3, &offset, sizeof(offset), 0);
			send_user(arg4, &len, sizeof(len), 0);

			ocall_syscall();

			break;
		}

		case __NR_dup3:		// dup3(292)
		{
			long fd_old = a1;
			long fd_new = a2;
			long flags = a3;

			send_user(arg1, &fd_old, sizeof(fd_old), 0);
			send_user(arg2, &fd_new, sizeof(fd_new), 0);
			send_user(arg3, &flags, sizeof(flags), 0);

			ocall_syscall();

			break;
		}

		case __NR_prlimit64:	// prlimit64(302)
		{
			pid_t pid = (pid_t)a1;
			long resource = a2;
			struct rlimit *new_limit = (struct rlimit*)a3;
			struct rlimit *old_limit = (struct rlimit*)a4;

			send_user(arg1, &pid, sizeof(pid), 0);
			send_user(arg2, &resource, sizeof(resource), 0);
			if(new_limit == NULL){
				*(long*)arg3 = 0;
			} else {
				send_user(arg5, new_limit, sizeof(*new_limit), 0);
				send_user(arg3, &arg5, sizeof(void*), 0);
			}

			if(old_limit == NULL)
				*(long*)arg4 = 0;
			else
				send_user(arg4, &arg6, sizeof(void*), 0);

			ocall_syscall();

			if(old_limit != NULL){
				recv_user(arg6, old_limit, sizeof(*old_limit), 0);
			}


			break;

		}

		case __NR_syncfs:	// syncfs(306)
		{
			long fd = a1;

			send_user(arg1, &fd, sizeof(fd), 0);

			ocall_syscall();

			break;
		}

		default:
		{
			syscall_wrap_defined = 0;
			*(long*)arg0 = -1;
			break;
		}

	}
	long ret_val = *(long*)arg0;
  // sgx_thread_mutex_unlock (&syscall_mutex);
	syscall_lock = 0;

	if(syscall_wrap_defined == 0)
		printf("syscall_wrap not defined %ld\n", n);
	return ret_val;
}
