######## SGX SDK Settings ########

SGX_SDK ?= /opt/intel/sgxsdk
SGX_MODE ?= HW
SGX_ARCH ?= x64
SGX_DEBUG ?= 1

CC = musl-gcc
MUSL_SYSTEM = /usr/local/musl
MUSL_ENCLAVE = ../musl_encl# location of enclave musl
BZIP2_ENCLAVE = enclave/bzip2

ifeq ($(shell getconf LONG_BIT), 32)
	SGX_ARCH := x86
else ifeq ($(findstring -m32, $(CXXFLAGS)), -m32)
	SGX_ARCH := x86
endif

ifeq ($(SGX_ARCH), x86)
	SGX_COMMON_CFLAGS := -m32
	SGX_LIBRARY_PATH := $(SGX_SDK)/lib
	SGX_ENCLAVE_SIGNER := $(SGX_SDK)/bin/x86/sgx_sign
	SGX_EDGER8R := $(SGX_SDK)/bin/x86/sgx_edger8r
else
	SGX_COMMON_CFLAGS := -m64
	SGX_LIBRARY_PATH := $(SGX_SDK)/lib64 # we are using our custom build
	SGX_ENCLAVE_SIGNER := $(SGX_SDK)/bin/x64/sgx_sign
	SGX_EDGER8R := $(SGX_SDK)/bin/x64/sgx_edger8r
endif

ifeq ($(SGX_DEBUG), 1)
ifeq ($(SGX_PRERELEASE), 1)
$(error Cannot set SGX_DEBUG and SGX_PRERELEASE at the same time!!)
endif
endif

ifeq ($(SGX_DEBUG), 1)
        SGX_COMMON_CFLAGS += -O0 -g
else
        SGX_COMMON_CFLAGS += -O0
endif

######## App Settings ########

ifneq ($(SGX_MODE), HW)
	urts_library_name := sgx_urts_sim
else
	urts_library_name := sgx_urts
endif

ifeq ($(SGX_DEBUG), 1)
        app_c_flags += -DDEBUG -UNDEBUG -UEDEBUG
else ifeq ($(SGX_PRERELEASE), 1)
        app_c_flags += -DNDEBUG -DEDEBUG -UDEBUG
else
        app_c_flags += -DNDEBUG -UEDEBUG -UDEBUG
endif


ifneq ($(SGX_MODE), HW)
	app_link_flags += -lsgx_uae_service_sim
else
	app_link_flags += -lsgx_uae_service
endif

app_src := $(wildcard app/sample_app/*.cpp) $(wildcard app/shim_layer/*.cpp) 
app_include_paths := -I$(SGX_SDK)/include -I$(MUSL_SYSTEM)/include -Iapp -Iapp/shim_layer
app_c_flags := $(SGX_COMMON_CFLAGS) -fPIC -Wno-attributes $(app_include_paths)
app_cpp_flags := $(app_c_flags) -nostdinc -std=c++11
app_link_flags := $(SGX_COMMON_CFLAGS) -L$(SGX_LIBRARY_PATH) -l$(urts_library_name) -lpthread
app_cpp_objects := $(app_src:.cpp=.o)

app_name := a.out

######## Enclave Settings ########

ifneq ($(SGX_MODE), HW)
	trts_library_name := sgx_trts_sim
	service_library_name := sgx_tservice_sim
else
	trts_library_name := sgx_trts
	service_library_name := sgx_tservice
endif
crypto_library_name := sgx_tcrypto

CC_BELOW_4_9 := $(shell expr "`$(CC) -dumpversion`" \< "4.9")
ifeq ($(CC_BELOW_4_9), 1)
	enclave_c_flags := $(SGX_COMMON_CFLAGS) -nostdinc -fvisibility=hidden -fpie -ffunction-sections -fdata-sections -fstack-protector
else
	enclave_c_flags := $(SGX_COMMON_CFLAGS) -nostdinc -fvisibility=hidden -fpie -ffunction-sections -fdata-sections -fstack-protector-strong
endif

enclave_cpp_files := $(wildcard enclave/test/*.cpp) $(wildcard enclave/sample_app/*.cpp) \
	$(wildcard enclave/shim_layer/*.cpp)

enclave_include_paths := -I$(SGX_SDK)/include \
	-I$(MUSL_SYSTEM)/include \
	-I$(SGX_SDK)/include/libcxx \
	-Ienclave \
	-Ienclave/shim_layer \
	-I$(BZIP2_ENCLAVE)

enclave_c_flags += $(enclave_include_paths)
enclave_cpp_flags := $(enclave_c_flags) -std=c++11 -nostdinc++

enclave_link_flags := $(SGX_COMMON_CFLAGS) -Wl,--no-undefined -nostdlib -nodefaultlibs \
	-nostartfiles -L$(SGX_LIBRARY_PATH) \
	-L$(MUSL_ENCLAVE)/lib \
	-Lenclave/shim_layer \
	-L$(BZIP2_ENCLAVE) \
	-Wl,--whole-archive -l$(trts_library_name) -Wl,--no-whole-archive \
	-Wl,--start-group -lsgx_tstdc -lc -lsgx_tcxx \
	-l$(crypto_library_name) \
	-l$(service_library_name) \
	-l:libbz2.a \
	-Wl,--end-group \
	-Wl,-Bstatic -Wl,-Bsymbolic -Wl,--no-undefined \
	-Wl,-pie,-eenclave_entry -Wl,--export-dynamic  \
	-Wl,--defsym,__ImageBase=0 -Wl,--gc-sections   \
	-Wl,--version-script=enclave/enclave.lds

enclave_cpp_objects := $(enclave_cpp_files:.cpp=.o)
enclave_name := enclave.so
signed_enclave_name := enclave.signed.so
enclave_config_file := enclave/enclave.config.xml

ifeq ($(SGX_MODE), HW)
ifeq ($(SGX_DEBUG), 1)
	build_mode = HW_DEBUG
else ifeq ($(SGX_PRERELEASE), 1)
	build_mode = HW_PRERELEASE
else
	build_mode = HW_RELEASE
endif
else
ifeq ($(SGX_DEBUG), 1)
	build_mode = SIM_DEBUG
else ifeq ($(SGX_PRERELEASE), 1)
	build_mode = SIM_PRERELEASE
else
	build_mode = SIM_RELEASE
endif
endif


.PHONY: all run

ifeq ($(build_mode), HW_RELEASE)
all: .config_$(build_mode)_$(SGX_ARCH) $(app_name) $(enclave_name)
	@echo "The project has been built in release hardware mode."
	@echo "Please sign the $(enclave_name) first with your signing key before you run the $(app_name) to launch and access the enclave."
	@echo "To sign the enclave use the command:"
	@echo "   $(SGX_ENCLAVE_SIGNER) sign -key <your key> -enclave $(enclave_name) -out <$(signed_enclave_name)> -config $(enclave_config_file)"
	@echo "You can also sign the enclave using an external signing tool."
	@echo "To build the project in simulation mode set SGX_MODE=SIM. To build the project in prerelease mode set SGX_PRERELEASE=1 and SGX_MODE=HW."
else
all: .config_$(build_mode)_$(SGX_ARCH) $(app_name) $(signed_enclave_name)
ifeq ($(build_mode), HW_DEBUG)
	@echo "The project has been built in debug hardware mode."
else ifeq ($(build_mode), SIM_DEBUG)
	@echo "The project has been built in debug simulation mode."
else ifeq ($(build_mode), HW_PRERELEASE)
	@echo "The project has been built in pre-release hardware mode."
else ifeq ($(build_mode), SIM_PRERELEASE)
	@echo "The project has been built in pre-release simulation mode."
else
	@echo "The project has been built in release simulation mode."
endif
endif

run: all
ifneq ($(build_mode), HW_RELEASE)
	@$(CURDIR)/$(app_name)
	@echo "RUN  =>  $(app_name) [$(SGX_MODE)|$(SGX_ARCH), OK]"
endif

######## App Objects ########

app/enclave_u.c: $(SGX_EDGER8R) enclave/enclave.edl
	@cd app && $(SGX_EDGER8R) --untrusted ../enclave/enclave.edl --search-path ../enclave --search-path $(SGX_SDK)/include
	@echo "GEN  =>  $@"

app/enclave_u.o: app/enclave_u.c
	@$(CC) $(app_c_flags) -c $< -o $@
	@echo "CC   <=  $<"

app/%.o: app/%.cpp
	@$(CXX) $(app_cpp_flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(app_name): app/enclave_u.o $(app_cpp_objects)
	@$(CXX) $^ -o $@ $(app_link_flags)
	@echo "LINK =>  $@"

.config_$(build_mode)_$(SGX_ARCH):
	@rm -f .config_* $(app_name) $(enclave_name) $(signed_enclave_name) $(app_cpp_objects) app/enclave_u.* $(enclave_cpp_objects) enclave/enclave_t.*
	@touch .config_$(build_mode)_$(SGX_ARCH)

######## Enclave Objects ########

enclave/enclave_t.c: $(SGX_EDGER8R) enclave/enclave.edl
	@cd enclave && $(SGX_EDGER8R) --trusted ../enclave/enclave.edl --search-path ../enclave --search-path $(SGX_SDK)/include
	@echo "GEN  =>  $@"

enclave/enclave_t.o: enclave/enclave_t.c
	@$(CC) $(enclave_c_flags) -c $< -o $@
	@echo "CC   <=  $<"

enclave/%.o: enclave/%.cpp
	@$(CXX) $(enclave_cpp_flags) -c $< -o $@
	@echo "CXX  <=  $<"

$(enclave_name): enclave/enclave_t.o $(enclave_cpp_objects) $(bzip2_obj)
	@$(CXX) $^ -o $@ $(enclave_link_flags)
	@echo "LINK =>  $@"
	@echo $^

$(signed_enclave_name): $(enclave_name)
	@$(SGX_ENCLAVE_SIGNER) sign -key enclave/enclave_private.pem -enclave $(enclave_name) -out $@ -config $(enclave_config_file)
	@echo "SIGN =>  $@"

.PHONY: clean

clean:
	@rm -f .config_* $(app_name) $(enclave_name) $(signed_enclave_name) $(app_cpp_objects) app/enclave_u.* $(enclave_cpp_objects) enclave/enclave_t.*

build_msg:
	echo $(MUSL_SYSTEM)
